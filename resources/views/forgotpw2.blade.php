<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reset Password</title>

    <!-- Custom fonts for this template-->
    <link href="{{url('template2/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{url('template2/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-md-6">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div>

                            @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                            @elseif (session('failedPass'))
                            <div class="alert alert-danger">
                                {{ session('failedPass') }}
                            </div>
                            @endif

                            <div class="col-md-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Reset Password</h1>
                                    </div>


                                    @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                    @endif

                                    <form class="user" method="POST" action="{{ route('email.resetpw')}}" enctype="multipart/form-data">
                                        @csrf
                                        @method('post')

                                        <div class="form-group">
                                            Email
                                            <input type="email" class="form-control form-control-user" id="email" name="email" placeholder="Masukan Email Anda...">
                                        </div>

                                        <div class="form-group">
                                            Password Baru
                                            <input type="sandi-baru" class="form-control form-control-user" id="sandi-baru" name="newPassword" placeholder="Kata Sandi Baru...">

                                        </div>

                                        <div class="form-group">
                                            Konfirmasi Password Baru
                                            <input type="konfirmasi" class="form-control form-control-user" id="konfirmasi" name="confirmPassword" placeholder="Konfirmasi Kata Sandi Baru...">

                                        </div>


                                        <button class="btn btn-primary btn-user btn-block">Reset Password</button>
                                        <a class="row justify-content-center mt-3" href="{{ url('/')}}">Login</a>

                                    </form>
                                    <hr>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </col-md-8>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{url('template2/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{url('template2/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{url('template2/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{url('template2/js/sb-admin-2.min.js')}}"></script>

</body>

</html>