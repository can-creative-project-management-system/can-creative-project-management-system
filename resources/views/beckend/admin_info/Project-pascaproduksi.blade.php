@extends('layouts/AdminInfo')

@section('title', 'Project Managament System - Admin Info')
@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Data Project Pasca - Produksi
                                        <div class="page-title-subheading">Tabel dibawah ini merupakan list projek setelah masa Produksi.
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                        </div>           
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Data Projek Pasca-Produksi</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Project</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                           
                                            
                                            <tbody>
                                                @foreach ($project as $no => $fb)
                                                <tr>
                                                    <th>{{$no+1}}</th>
                                                    <th>{{$fb->nama}}</th>
                                                    <th>
                                                        @if ($fb->status == "Pra")
                                                            <a href="{{route('AdminInfo.project.gantistatus', $fb->id_project)}}">
                                                                <button class="btn-sm btn-warning">Pra Produksi</button>
                                                            </a>
                                                        @elseif ($fb->status == "Produksi")
                                                        <a href="{{route('AdminInfo.project.gantistatus', $fb->id_project)}}">
                                                            <button class="btn-sm btn-success">Produksi</button>
                                                        </a>
                                                        @else
                                                        <a href="{{route('AdminInfo.project.gantistatus', $fb->id_project)}}">
                                                            <button class="btn-sm btn-info">Pasca Produksi</button>
                                                        </a>
                                                        @endif
                                                    </th>
                                                    <th>
                                                        <a href="AdminInfo.project.detailproject.{{$fb->id_project}}" class="btn btn-success">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>
                                                
                                                        <a href="AdminInfo.report.{{$fb->id_project}}" class="btn btn-info">
                                                            <i class="fa fa-tasks" aria-hidden="true"></i>
                                                        </a>
                                                        
                                                    </th>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <!-- /.container-fluid -->
                        
                            <!-- Bootstrap core JavaScript-->
                            <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
                            <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                        
                            <!-- Core plugin JavaScript-->
                            <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
                        
                            <!-- Custom scripts for all pages-->
                            <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
                        
                            <!-- Page level plugins -->
                            <script src="{{url('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
                            <script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        
                            <!-- Page level custom scripts -->
                            <script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
                            <br><br><br><br><br><br><br><br>
                    @endsection