@extends('layouts/main')

@section('title', 'Project Managament System - Super Admin')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-help1 icon-gradient bg-night-fade">
                                        </i>
                                    </div>
                                    <div>Data Informasi
                                        <div class="page-title-subheading">Tabel dibawah ini merupakan list data informasi terkait user guide penggunaan aplikasi.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">
                                    <div class="d-inline-block dropdown">
                                        <a href="{{route('superadmin.tambah_info')}}">
                                            <button type="button" aria-expanded="false" class="btn-shadow btn btn-info">
                                                <span class="btn-icon-wrapper pr-2 opacity-7">
                                                    <i class="fa fa-business-time fa-w-20"></i>
                                                </span>
                                                Tambah Info
                                            </button>
                                        </a>
                                    </div>
                                </div> 
                            </div>
                        </div>           
                        @if (session('popup'))
                        <div class="alert alert-success fade show" role="alert">
                            {{ session('popup') }}
                        </div>
                        @endif
                            <div class="card shadow mb-4">
                                
                                <div class="card-body">
                                    <div class="table-hover">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Module</th>
                                                    <th>Keterangan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                           
                                            
                                            <tbody>
                                                @foreach ($info as $fb)
                                                <tr>
                                                    <td><p>{{$fb->module}}</p></td>
                                                    <td><p>{{$fb->keterangan}}</p></td>
                                                    <td>
                                                        @if ($fb->status == "Aktif")
                                                            <a href="{{route('superadmin.info.ubah', $fb->id)}}" class="mb-2 mr-2 btn-transition btn btn-outline-success">
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </a>
                                                        @else
                                                            <a href="{{route('superadmin.info.ubah', $fb->id)}}" class="mb-2 mr-2 btn-transition btn btn-outline-warning">
                                                                <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                                            </a>
                                                        @endif
                                                        <a href="{{route('superadmin.info.edit', $fb->id)}}" class="mb-2 mr-2 btn-transition btn btn-outline-info">
                                                            <i class="fa fa-pen" aria-hidden="true"></i>
                                                        </a>
                                                         <form action="{{url('superadmin.info.delete.'.$fb->id)}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                                                            @method('delete')
                                                            @csrf
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-danger"><i class="fas fa-trash-alt"></i></button>
                                                            @csrf
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <!-- /.container-fluid -->
                        
                            <!-- Bootstrap core JavaScript-->
                            <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
                            <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                        
                            <!-- Core plugin JavaScript-->
                            <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
                        
                            <!-- Custom scripts for all pages-->
                            <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
                        
                            <!-- Page level plugins -->
                            <script src="{{url('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
                            <script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        
                            <!-- Page level custom scripts -->
                            <script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
                            <br><br><br><br><br><br><br><br>
                    @endsection