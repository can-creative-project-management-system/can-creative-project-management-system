@extends('layouts/main')

@section('title', 'Ganti Status Tiket - Super Admin')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="fa fa-database icon-gradient bg-ripe-malin">
                                        </i>
                                    </div>
                                    <div>Edit Informasi User Guide
                                        <div class="page-title-subheading">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
                        @if (session('popup'))
                        <div class="alert alert-success fade show" role="alert">
                            {{ session('popup') }}
                        </div>
                        @endif
                       
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title"></h5>
                                        <form class="" action="{{ route('superadmin.info.save', $info->id)}}" method="POST">
                                           @csrf
                                           @method('patch')
                        
                                            <div class="position-relative form-group">
                                                <h5 class="card-title">Module</h5>
                                                <textarea name="module" id="module" class="form-control">{{$info->module}}</textarea>
                                            </div>
                                            <div class="position-relative form-group">
                                                <h5 class="card-title">Keterangan</h5>
                                                <textarea name="keterangan" id="keterangan" class="form-control">{{$info->keterangan}}</textarea>
                                            </div>
                                                
                                                <button class="mt-2 btn btn-primary">Ganti Status</button>
                                        </form>
                                    </div>
                                </div>
                                
                    @endsection