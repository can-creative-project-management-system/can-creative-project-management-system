@extends('layouts/main')

@section('title', 'Project Managament System - Super Admin')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Data Project Pra - Produksi
                    <div class="page-title-subheading">Dibawah ini merupakan list data Project Pra-Produksi.
                    </div>
                </div>
            </div>
           
        </div>
    </div>
    <div class="col-lg-12">
        <div class="main-card mb-12 card">
            <div class="card-body">
                <h5 class="card-title">Table Data Project Pra-Produksi</h5>
                <div class="table-responsive">
                    <table class="mb-3 table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Status</th>
                                <th>Aksi</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0; ?>
                            @foreach($proyek as $fb)
                            <?php $no++; ?>
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{$fb -> nama }}</td>
                                <td>
                                    @if ($fb->status == "Pra")
                                        <a href="{{route('superadmin.project.gantistatus', $fb->id_project)}}">
                                            <button class="btn-sm btn-warning">Pra Produksi</button>
                                        </a>
                                    @elseif ($fb->status == "Produksi")
                                        <a href="{{route('superadmin.project.gantistatus', $fb->id_project)}}">
                                            <button class="btn-sm btn-success">Produksi</button>
                                        </a>
                                    @else
                                        <a href="{{route('superadmin.project.gantistatus', $fb->id_project)}}">
                                            <button class="btn-sm btn-info">Pasca Produksi</button>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="superadmin.project.detailproject.{{$fb->id_project}}" class="btn btn-success">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a href="superadmin.project.client.{{$fb->id_project}}" class="btn btn-warning">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <a href="superadmin.report.{{$fb->id_project}}" class="btn btn-info">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                    </a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br><br><br><br><br>
    @endsection