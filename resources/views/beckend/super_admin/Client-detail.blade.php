@extends('layouts/main')

@section('title', 'Project Managament System - Super Admin')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Detail Data Client
                                        <div class="page-title-subheading">
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                        </div>           
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Detail Data Admin</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table" id="dataTable" width="100%" cellspacing="0">
                                            @foreach($users as $DataAdmin)
                                            @if ($DataAdmin->file == Null)
                                            
                                            @else
                                            <tr>
                                                <td style="font-size: 20px;color: #000000; "><img width="300px" src="{{ url('logo/'.$DataAdmin->file) }}"></td>
                                            </tr>      
                                            @endif
                                            <tr>
                                                <th style="">Nama</th>
                                                <th>:</th>
                                                <td style="">{{ $DataAdmin->name }}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Email</th>
                                                <th>:</th>
                                                <td style="">{{ $DataAdmin->email }}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Tlp</th>
                                                <th>:</th>
                                                <td style="">{{ $DataAdmin->tlp }}</td>
                                            </tr>

                                            <tr>
                                                <th style="">Alamat</th>
                                                <th>:</th>
                                                <td style="">{{ $DataAdmin->alamat}}</td>
                                            </tr>

                                            <tr>
                                                <th style="">Status</th>
                                                <th>:</th>
                                                <td style="">
                                                    @if($DataAdmin->status == "Nonaktif")
                                                    <a href="" class="btn btn-danger">
                                                        Nonaktif
                                                    </a>
                                                    @else
                                                    <a href="{{route('superadmin.user.gantistatus', $DataAdmin->id)}}" class="btn btn-info">
                                                        Aktif
                                                    </a>
                                                    @endif
                                                </td>
                                                
                                            </tr>
                                            
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <!-- /.container-fluid -->
                        
                            <!-- Bootstrap core JavaScript-->
                            <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
                            <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                        
                            <!-- Core plugin JavaScript-->
                            <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
                        
                            <!-- Custom scripts for all pages-->
                            <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
                        
                            <!-- Page level plugins -->
                            <script src="{{url('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
                            <script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        
                            <!-- Page level custom scripts -->
                            <script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
                            <br><br><br><br><br><br><br><br>
                    @endsection