@extends('layouts/main')

@section('title', 'Project Managament System - Super Admin')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Data Ticket
                    
                </div>
            </div>
            
        </div>
    </div>            
    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
        <li class="nav-item">
            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                <span>All</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                <span>Un-Opened</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2">
                <span>Open</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-3">
                <span>In-Progress</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-4">
                <span>On-Hold</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-5">
                <span>Answered</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-6">
                <span>Closed</span>
            </a>
        </li>
    </ul>
    @if (session('popup'))
        <div class="alert alert-success fade show" role="alert">
            {{ session('popup') }}
        </div>
    @endif 
    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active col-md-12" id="tab-content-0" role="tabpanel">
            <div class="row">
                <div class="card shadow mb-12 col-md-12">
                    <div class="card-header py-12">
                        <h6 class="m-0 font-weight-bold text-primary">Semua Tiket Masuk</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Project</th>
                                        <th>Subject</th>
                                        <th>Division</th>
                                        <th>Last Update</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                               
                                
                                <tbody>
                                    @foreach ($ticket as $no => $fb)
                                    <tr>
                                        <th>{{$no+1}}</th>
                                        <th>{{$fb->nama}}</th>
                                        <th>{{$fb->subject}}</th>
                                        <th>{{$fb->divisi}}</th>
                                        <th>{{$fb->updated_at}}</th>
                                       
                                        <th>
                                            <a href="superadmin.tiket.detailtiket.{{$fb->id_ticket}}" class="btn btn-success">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            
                                            
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
            <div class="row">
                <div class="card shadow mb-12 col-md-12">
                    <div class="card-header py-12">
                        <h6 class="m-0 font-weight-bold text-primary">Tiket Baru Diterima</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Project</th>
                                        <th>Subject</th>
                                        <th>Division</th>
                                        <th>Last Update</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                               
                                
                                <tbody>
                                    <?php 
                                    $krisan = DB::table('tickets')->where('status_penanganan', '=', 'Un-Opened')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                                @foreach ($krisan as $no => $fb)
                                    <tr>
                                        <th>{{$no+1}}</th>
                                        <th>{{$fb->nama}}</th>
                                        <th>{{$fb->subject}}</th>
                                        <th>{{$fb->divisi}}</th>
                                        <th>{{$fb->updated_at}}</th>
                                       
                                        <th>
                                            <a href="superadmin.tiket.detailtiket.{{$fb->id_ticket}}" class="btn btn-success">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            
                                            
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>   

        </div>

        <div class="tab-pane tabs-animation fade" id="tab-content-2" role="tabpanel">
            <div class="row">
                <div class="card shadow mb-12 col-md-12">
                    <div class="card-header py-12">
                        <h6 class="m-0 font-weight-bold text-primary">Tiket Sudah Dibuka</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Project</th>
                                        <th>Subject</th>
                                        <th>Division</th>
                                        <th>Last Update</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                               
                                
                                <tbody>
                                    <?php 
                                    $krisan = DB::table('tickets')->where('status_penanganan', '=', 'Open')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                                @foreach ($krisan as $no => $fb)
                                    <tr>
                                        <th>{{$no+1}}</th>
                                        <th>{{$fb->nama}}</th>
                                        <th>{{$fb->subject}}</th>
                                        <th>{{$fb->divisi}}</th>
                                        <th>{{$fb->updated_at}}</th>
                                       
                                        <th>
                                            <a href="superadmin.tiket.detailtiket.{{$fb->id_ticket}}" class="btn btn-success">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            
                                            
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-3" role="tabpanel">
            <div class="row">
                <div class="card shadow mb-12 col-md-12">
                    <div class="card-header py-12">
                        <h6 class="m-0 font-weight-bold text-primary">Tiket sedang dalam Progress</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Project</th>
                                        <th>Subject</th>
                                        <th>Division</th>
                                        <th>Last Update</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                               
                                
                                <tbody>
                                    <?php 
                                    $krisan = DB::table('tickets')->where('status_penanganan', '=', 'In-Progress')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                                @foreach ($krisan as $no => $fb)
                                    <tr>
                                        <th>{{$no+1}}</th>
                                        <th>{{$fb->nama}}</th>
                                        <th>{{$fb->subject}}</th>
                                        <th>{{$fb->divisi}}</th>
                                        <th>{{$fb->updated_at}}</th>
                                       
                                        <th>
                                            <a href="superadmin.tiket.detailtiket.{{$fb->id_ticket}}" class="btn btn-success">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            
                                            
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-4" role="tabpanel">
            <div class="row">
                <div class="card shadow mb-12 col-md-12">
                    <div class="card-header py-12">
                        <h6 class="m-0 font-weight-bold text-primary">Tiket Tertahan</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Project</th>
                                        <th>Subject</th>
                                        <th>Division</th>
                                        <th>Last Update</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                               
                                
                                <tbody>
                                    <?php 
                                    $krisan = DB::table('tickets')->where('status_penanganan', '=', 'On-Hold')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                                @foreach ($krisan as $no => $fb)
                                    <tr>
                                        <th>{{$no+1}}</th>
                                        <th>{{$fb->nama}}</th>
                                        <th>{{$fb->subject}}</th>
                                        <th>{{$fb->divisi}}</th>
                                        <th>{{$fb->updated_at}}</th>
                                       
                                        <th>
                                            <a href="superadmin.tiket.detailtiket.{{$fb->id_ticket}}" class="btn btn-success">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            
                                            
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-5" role="tabpanel">
            <div class="row">
                <div class="card shadow mb-12 col-md-12">
                    <div class="card-header py-12">
                        <h6 class="m-0 font-weight-bold text-primary">Tiket Telah Dijawab</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Project</th>
                                        <th>Subject</th>
                                        <th>Division</th>
                                        <th>Last Update</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                               
                                
                                <tbody>
                                    <?php 
                                    $krisan = DB::table('tickets')->where('status_penanganan', '=', 'Answered')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                                @foreach ($krisan as $no => $fb)
                                    <tr>
                                        <th>{{$no+1}}</th>
                                        <th>{{$fb->nama}}</th>
                                        <th>{{$fb->subject}}</th>
                                        <th>{{$fb->divisi}}</th>
                                        <th>{{$fb->updated_at}}</th>
                                       
                                        <th>
                                            <a href="superadmin.tiket.detailtiket.{{$fb->id_ticket}}" class="btn btn-success">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            
                                            
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-6" role="tabpanel">
            <div class="row">
                <div class="card shadow mb-12 col-md-12">
                    <div class="card-header py-12">
                        <h6 class="m-0 font-weight-bold text-primary">Tiket Selesai</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Project</th>
                                        <th>Subject</th>
                                        <th>Division</th>
                                        <th>Last Update</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                               
                                
                                <tbody>
                                    <?php 
                                    $krisan = DB::table('tickets')->where('status_penanganan', '=', 'Closed')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                                @foreach ($krisan as $no => $fb)
                                    <tr>
                                        <th>{{$no+1}}</th>
                                        <th>{{$fb->nama}}</th>
                                        <th>{{$fb->subject}}</th>
                                        <th>{{$fb->divisi}}</th>
                                        <th>{{$fb->updated_at}}</th>
                                       
                                        <th>
                                            <a href="superadmin.tiket.detailtiket.{{$fb->id_ticket}}" class="btn btn-success">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            
                                            
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>
                        <!-- /.container-fluid -->
                        
                            <!-- Bootstrap core JavaScript-->
                            <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
                            <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                        
                            <!-- Core plugin JavaScript-->
                            <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
                        
                            <!-- Custom scripts for all pages-->
                            <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
                        
                            <!-- Page level plugins -->
                            <script src="{{url('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
                            <script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        
                            <!-- Page level custom scripts -->
                            <script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
                            <br><br><br><br><br><br><br><br>
@endsection