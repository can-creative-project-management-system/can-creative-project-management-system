@extends('layouts/main')

@section('title', 'Jawab Ticket - Super Admin')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="fa fa-database icon-gradient bg-ripe-malin">
                                        </i>
                                    </div>
                                    <div>Jawab Ticket
                                        <div class="page-title-subheading">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
                        @if (session('status'))
                        <div class="alert alert-success fade show" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                       
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                   
                                    <div class="card-body"><h5 class="card-title"></h5>
                                        <br>
                                        @foreach($Ticket as $T)
                                        <div class="position-relative">
                                            <div class="main-card mb-3 card">
                                                <div class="card-body">
                                                    <h5 class="card-title">Subject</h5>
                                                    <h6>{{$T->subject}}</h6>
                                                </div>
                                            </div>
                                            <div class="main-card mb-3 card">
                                                <div class="card-body">
                                                    <h5 class="card-title">Detail Ticket</h5>
                                                    <h6>{{$T->detail_ticket}}</h6>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <form class="" action="{{ route('superadmin.tiket.simpanjawab', $T->id_ticket)}}" method="POST">
                                           @csrf
                                           @method('patch')
                                           <div class="main-card mb-3 card">
                                                <div class="card-body">
                                                    <div class="position-relative form-group"><label for="exampleAddress" class=""><h5 class="card-title">Jawab Ticket</h5></label>
                                                        <textarea name="jawaban" id="jawaban" class="form-control"></textarea>
                                                        @error('jawaban')<div class="invalid-feedback">{{$message}}</div> @enderror
                                                    </div>
                                                </div>
                                            </div>
                                           
                                                
                                                <button class="mt-2 btn btn-primary">Tambah</button>
                                        </form>
                                        @endforeach
                                    </div>
                                </div>
                                
                    @endsection