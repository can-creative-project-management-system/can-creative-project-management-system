@extends('layouts/main')

@section('title', 'Edit Report - Super Admin')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="fa fa-database icon-gradient bg-ripe-malin">
                                        </i>
                                    </div>
                                    <div>Edit Report
                                        <div class="page-title-subheading">.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
                        @if (session('status'))
                        <div class="alert alert-success fade show" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                       
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title"></h5>
                                        <form class="" action="{{ route('superadmin.report.simpanedit', .$report->id_report)}}" method="POST">
                                           @csrf
                                           @method('patch')
                                             <div class="position-relative form-group"><label for="exampleAddress" class=""></label>
                                                <input type="hidden" name="updated_at" value="{{$report->updated_at}}">
                                                <div class="position-relative form-group"><label for="exampleAddress2" class="">Subject</label>
                                                    <input name="subject" id="subject" placeholder="subject" type="text" class="form-control" value="{{$report->subject}}">
                                                </div>
                                                <div class="position-relative form-group"><label for="exampleAddress" class="">Change Log</label>
                                                    <textarea name="changelog" id="changelog" class="form-control"></textarea>
                                                    @error('changelog')<div class="invalid-feedback">{{$message}}</div> @enderror
                                                </div>
                                                <div class="position-relative form-group"><label for="exampleAddress2" class="">Link</label>
                                                    <input name="link" id="link" placeholder="link" type="text" class="form-control" {{$report->link}}>
                                                </div>
                                                <div class="position-relative form-group"><label for="exampleAddress2" class="">File</label>
                                                    <input type="file" multiple name="file" id="file" class="form-control">
                                                    @error('file')<div class="invalid-feedback">{{$message}}</div> @enderror
                                                </div>
                        
                        
                                            </div>
                                                
                                                <button class="mt-2 btn btn-primary">Tambah</button>
                                        </form>
                                    </div>
                                </div>
                                
                    @endsection