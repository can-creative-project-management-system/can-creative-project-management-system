@extends('layouts/main')

@section('title', 'Data Project - Super Admin')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="tab-content">
            <div class="tab-pane tabs-animation fade show active" id="tab-content-1" role="tabpanel">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{route('superadmin.tambah_project')}}">
                            <div class="main-card mb-3 card">
                                <img width="317px" height="210" src="{{url('template/assets/images/adproject.png')}}" alt="Card image cap" class="card-img-top">
                                <div class="card-body">
                                    <center>
                                    <h5 class="card-title">Tambah Project</h5></center>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="col-md-4">
                        <a href="{{route('superadmin.project.praproduksi')}}">
                            <div class="main-card mb-3 card">
                                <img width="317px" height="210" src="{{url('template/assets/images/projectplanning.png')}}" alt="Card image cap" class="card-img-top">
                                <div class="card-body">
                                    <center><h5 class="card-title">Pra-Produksi</h5></center>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="col-md-4">
                        <a href="{{route('superadmin.project.produksi')}}">
                            <div class="main-card mb-3 card">
                                <img width="317px" height="210" src="{{url('template/assets/images/produksi.png')}}" alt="Card image cap" class="card-img-top">
                                <div class="card-body">
                                    <center><h2 class="card-title">Produksi</h2></center>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="col-md-4">
                        <a href="{{route('superadmin.project.pascaproduksi')}}">
                            <div class="main-card mb-3 card">
                                <img width="317px" height="210" src="{{url('template/assets/images/pascaproduksi.png')}}" alt="Card image cap" class="card-img-top">
                                <div class="card-body">
                                   <center> <h2 class="card-title">Pasca-PRODUKSI</h2></center>
                                </div>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection