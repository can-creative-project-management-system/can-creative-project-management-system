@extends('layouts/main')

@section('title', 'Can Creative Project Managament System - Super Admin')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fa fa-database icon-gradient bg-ripe-malin">
                    </i>
                </div>
                <div>Buat Project Baru
                    <div class="page-title-subheading">Isi form dibawah ini untuk membuat project baru.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Buat Project Baru</h5>

                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status')}}
                    </div>
                    @endif
                    <form method="POST" action="{{ url ('superadmin.simpan_project')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="position-relative form-group"><label for="" class="">Nama</label>
                                    <input name="nama" id="nama" placeholder="Nama Project" type="text" class="form-control"></div>
                                @error('nama')<div class="invalid-feedback">{{$message}}</div> @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group"><label for="examplePassword11" class="">Due Project</label>
                                    <input name="due" id="due" placeholder="Due Project" type="date" class="form-control"></div>
                                @error('due')<div class="invalid-feedback">{{$message}}</div> @enderror
                            </div>
                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress" class="">Detail Project</label>
                            <textarea name="detail" id="detail" class="form-control"></textarea>
                            @error('detail')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>
                        @error('status')<div class="invalid-feedback">{{$message}}</div> @enderror
                        <div class="position-relative form-group"><label for="status" class="">Status Project</label><select type="select" id="status" name="status" class="custom-select">
                            <option selected>Status Project</option>
                            <option value="Pra">Pra Produksi</option>
                            <option value="Produksi">Produksi</option>
                            <option value="Pasca">Pasca Produksi </option>
                            </select></div>
                        <div class="position-relative form-group"><label for="exampleAddress2" class="">Lampiran</label>
                            <input name="mou[]" id="mou[]" placeholder="Lampiran" type="file" class="form-control">
                            <!-- @error('mou')<div class="invalid-feedback">{{$message}}</div> @enderror -->
                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress2" class="">Logo</label>
                            <input name="logo" id="logo" placeholder="Logo" type="file" class="form-control">
                            @error('logo')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>
                        
                        <button class="mt-2 btn btn-primary">Tambah</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection