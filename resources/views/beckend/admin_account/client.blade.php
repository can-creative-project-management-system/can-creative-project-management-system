@extends('layouts/AdminAccount')

@section('title', 'Data Admin - Admin Account')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Data Client
                    <div class="page-title-subheading">Dibawah ini merupakan list data Client.
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fa fa-star"></i>
                </button>

            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="main-card mb-12 card">
            <div class="card-body">
                <h5 class="card-title">Table Data Client</h5>
                <div class="table-responsive">
                    <table class="mb-3 table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0; ?>
                            @foreach($client as $adm_divisi)
                            <?php $no++; ?>
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{$adm_divisi -> name }}</td>
                                <td>{{$adm_divisi -> email}}</td>
                                <td>
                                    @if ($adm_divisi->status == "Nonaktif")
                                        <a href="{{route('AdminAccount.users.gantistatus', $adm_divisi->id)}}">
                                            <button class="btn-sm btn-danger">Nonaktif</button>
                                        </a>
                                    @else
                                        <a href="{{route('AdminAccount.users.gantistatus', $adm_divisi->id)}}">
                                            <button class="btn-sm btn-success">Aktif</button>
                                        </a>
                                    @endif
                                </td>


                                <td>
                                    <a href="{{route('AdminAccount.user.detail', $adm_divisi->id)}}" class="btn btn-success">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>

                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br><br><br><br><br>
    @endsection