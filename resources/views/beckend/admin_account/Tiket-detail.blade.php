@extends('layouts/AdminAccount')

@section('title', 'Can Creative Project Management System - Super Admin')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Detail Data Ticket
                                        <div class="page-title-subheading">Tabel dibawah ini merupakan detail Ticket.
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>           
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Detail Data Ticket</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table" id="dataTable" width="100%">
                                            @foreach($project as $DataAdmin)
                                            <tr>
                                                <th style="">Nama Project</th>
                                                <th> : </th>
                                                <td style="">{{ $DataAdmin->nama}}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Subject</th>
                                                <th> : </th>
                                                <td style="">{{ $DataAdmin->subject }}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Detail</th>
                                                <th> : </th>
                                                <td style="">{{ $DataAdmin->detail_ticket}}</td>
                                            </tr>

                                            <tr>
                                                <th style="">Divisi Tujuan</th>
                                                <th> : </th>
                                                <td style="">{{ $DataAdmin->divisi}}</td>
                                            </tr>

                                            <tr>
                                                <th style="">Urgent Status</th>
                                                <th> : </th>
                                                <td style="">
                                                    @if ($DataAdmin->urgent_status == "High")
                                                        <a href="">
                                                            <button class="btn-sm btn-danger">High</button>
                                                        </a>
                                                    @elseif ($DataAdmin->urgent_status == "Medium")
                                                        <a href="">
                                                            <button class="btn-sm btn-warning">Medium</button>
                                                        </a>
                                                    @else
                                                        <a href="">
                                                            <button class="btn-sm btn-info">Low</button>
                                                        </a>
                                                    @endif
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="">Penanganan</th>
                                                <th> : </th>
                                                <td style="">
                                                    @if ($DataAdmin->status_penanganan == "Un-Opened")
                                                        <a href="{{route('AdminAccount.tiket.gantistatus', $DataAdmin->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-warning">Unopened</button>
                                                        </a>
                                                    @elseif ($DataAdmin->status_penanganan == "Open")
                                                        <a href="{{route('AdminAccount.tiket.gantistatus', $DataAdmin->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-info">Open</button>
                                                        </a>
                                                    @elseif ($DataAdmin->status_penanganan == "In-Progress")
                                                        <a href="{{route('AdminAccount.tiket.gantistatus', $DataAdmin->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-alternate">In Progress</button>
                                                        </a>
                                                    @elseif ($DataAdmin->status_penanganan == "On-Hold")
                                                        <a href="{{route('AdminAccount.tiket.gantistatus', $DataAdmin->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-danger">On Hold</button>
                                                        </a>
                                                    @elseif ($DataAdmin->status_penanganan == "Answered")
                                                        <a href="{{route('AdminAccount.tiket.gantistatus', $DataAdmin->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-primary">Answered</button>
                                                        </a>
                                                    @else
                                                        <a href="{{route('AdminAccount.tiket.gantistatus', $DataAdmin->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-success">Closed</button>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="">Waktu Dibuat</th>
                                                <th> : </th>
                                                <td style="">{{ $DataAdmin->created_at}}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Terakhir Di-Update</th>
                                                <th> : </th>
                                                <td style="">{{ $DataAdmin->updated_at}}</td>
                                            </tr>
                                            
                                        </table>
                                        
                                        {{-- <tr>
                                            <th><a href="">
                                                <button type="button" aria-expanded="false" class="btn-shadow btn btn-info">
                                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                                        <i class="fa fa-business-time fa-w-20"></i>
                                                    </span>
                                                    Balas
                                                </button>
                                                </a>
                                            </th>
                                        </tr> --}}

                                        <a href="{{route('AdminAccount.Ticket')}}" class="btn btn-primary btn-sm">Back</a>
                                        <button class="btn btn-primary" id="btn-komentar-utama"><i class="fa fa-comment"></i> Komentar</button>
                
                
                
                                        <form action="" style="margin-top: 10px;display:none;margin-bottom:10px;" id="komentar-utama" method="POST">
                                            @csrf
                                            <input type="hidden" name="id_ticket" id="" value="{{$ticket->id_ticket}}">
                                            <input type="hidden" name="id_project" id="" value="{{$ticket->id_project}}">
                                            <input type="hidden" name="id_user" id="">
                                            <input type="hidden" name="id_admin">
                                            <input type="hidden" name="name" value="{{$ticket->name}}">
                                            <input type="hidden" name="parent" value="0">
                                            <textarea style="margin-top: 10px;" name="komentar" id="komentar-utama" cols="30" rows="4" class="form-control"></textarea>
                                            <input style="margin-top: 10px;" type="submit" class="btn btn-primary" value="Kirim"></input>
                                        </form>
                
                                        <br><br>
                                        <h3>Komentar</h3>
                                        <ul class="list-unstyled activity-list mt-3">
                                            <?php
                
                                            use Illuminate\Support\Facades\Session;
                                            use Illuminate\Support\Facades\DB;
                                            use Illuminate\Support\Facades\Request;
                                            use Illuminate\Support\Facades\URL;
                
                                            $arrURL = explode("http://", url()->current());
                                            $arr2URL = $arrURL[1];
                                            $arr3URL = explode(".", $arr2URL);
                                            $arrFix = explode(".",$arr3URL[1]);
                                            $komentar = DB::table('komentars')
                                                // ->leftJoin('posts', 'users.id', '=', 'posts.user_id')
                
                                                ->leftjoin('users', 'komentars.id_user', '=', 'users.id')
                                                ->leftjoin('admin', 'komentars.id_admin', '=', 'admin.id')
                                                ->join('tickets', 'komentars.id_ticket', '=', 'tickets.id_ticket')
                                                // ->select('komentars.*', 'users.name', 'admin.name')->groupBy(
                                                //     'komentars.id_user',
                                                //     'komentars.id_admin',
                                                //     'komentars.id',
                                                //     'komentars.id_admin',
                                                //     'komentars.id_project',
                                                //     'komentars.id_ticket',
                                                //     'users.name',
                                                //     'admin.name',
                                                //     'komentars.komentar',
                                                //     'komentars.parent',
                                                //     'komentars.created_at',
                                                //     'komentars.updated_at'
                                                // )
                                                ->select('komentars.*',  'admin.name AS aname', 'users.name AS uname')->distinct()
                                                ->where('komentars.id_ticket', $arrFix[2])
                                                ->get()
                
                
                
                                            ?>
                                            @foreach($komentar as $komentar)
                                            <li>
                                                <p>
                                                    @if ($komentar->id_admin != Null)
                                                    <a>{{$komentar->aname }}</a>
                                                    @else
                                                    <a>{{$komentar->uname }}</a>
                                                    @endif
                                                </p>
                                                <p>{{$komentar->komentar}}</p>
                
                                                <p> <span class="timestamp">{{$komentar->created_at}}</span>
                                                </p>
                                                
                
                                            </li>
                                            @endforeach
                                           
                                        </ul>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Jawaban Ticket</h6>
                                </div>
                                <div class="card-body">
                                    <div class="">
                                        <th>
                                          
                                        </th>
                                        <br><br><br>
                                    </div>
                                    <div>
                                        <th>
                                            <b>Best Regard,</b>
                                        </th>
                                        <br>
                                        <th>
                                            <b>Emma</b>
                                        </th>
                                        <br>
                                        <th>
                                            <b>Account Division</b>
                                        </th>
                                    </div>
                                </div>
                            </div>
                            
                           @endforeach
                        </div>
                        <!-- /.container-fluid -->
                        
                            <!-- Bootstrap core JavaScript-->
                            <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
                            <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                        
                            <!-- Core plugin JavaScript-->
                            <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
                        
                            <!-- Custom scripts for all pages-->
                            <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
                        
                            <!-- Page level plugins -->
                            <script src="{{url('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
                            <script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        
                            <!-- Page level custom scripts -->
                            <script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
                            <br><br><br><br><br><br><br><br>
                    @endsection
