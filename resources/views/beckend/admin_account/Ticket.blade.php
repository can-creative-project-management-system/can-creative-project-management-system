@extends('layouts/AdminAccount')

@section('title', 'Can Creative Project Management System - Super Admin')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Data Ticket
                    <div class="page-title-subheading">Dibawah ini merupakan list data ticket yang telah diterima.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
        <li class="nav-item">
            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                <span>All</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                <span>Un-Opened</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2">
                <span>Open</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-3">
                <span>In-Progress</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-4">
                <span>On-Hold</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-5">
                <span>Answered</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-6">
                <span>Closed</span>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">

            <div class="main-card mb-12 card">
                <div class="card-body">
                    <h5 class="card-title"> Data Ticket </h5>
                    <div class="table-responsive">
                        <table class="mb-12 table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Project</th>
                                    <th>Subject</th>
                                    <th>Division</th>
                                    <th>Urgent Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0; ?>
                                @foreach($ticket as $tickets)
                                <?php $no++; ?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{$tickets -> nama }}</td>
                                    <td>{{$tickets -> subject }}</td>
                                    <td>{{$tickets -> divisi }}</td>
                                    <td>{{$tickets -> urgent_status }}</td>


                                    <td>

                                        <a href="AdminAccount.tiket.detailtiket.{{$tickets->id_ticket}}" class="btn btn-success">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
            <div class="main-card mb-12 card">
                <div class="card-body">
                    <h5 class="card-title">Data Tiket yang belum dibuka</h5>
                    <div class="table-responsive">
                        <table class="mb-12 table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Project</th>
                                    <th>Subject</th>
                                    <th>Division</th>
                                    <th>Urgent Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <?php 
                            $ticket1 = DB::table('tickets')->where('status_penanganan', '=', 'Un-Opened')
                            ->join('project', 'tickets.id_project', '=', 'project.id_project')
                            ->select('tickets.*', 'project.nama', 'project.logo')
                            ->get();
                            ?>
                            <tbody>
                                <?php $no = 0; ?>
                                @foreach($ticket1 as $tickets)
                                <?php $no++; ?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{$tickets -> nama }}</td>
                                    <td>{{$tickets -> subject }}</td>
                                    <td>{{$tickets -> divisi }}</td>
                                    <td>{{$tickets -> urgent_status }}</td>


                                    <td>

                                        <a href="AdminAccount.tiket.detailtiket.{{$tickets->id_ticket}}" class="btn btn-success">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-2" role="tabpanel">
            <div class="main-card mb-12 card">
                <div class="card-body">

                    <h5 class="card-title">Data Ticket yang telah di buka</h5>
                    <div class="table-responsive">
                        <table class="mb-12 table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Project</th>
                                    <th>Subject</th>
                                    <th>Division</th>
                                    <th>Last Update</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>
                            <?php 
                                    $ticket2 = DB::table('tickets')->where('status_penanganan', '=', 'Open')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                            <tbody>
                                <?php $no = 0; ?>
                                @foreach($ticket2 as $tickets)
                                <?php $no++; ?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{$tickets -> nama }}</td>
                                    <td>{{$tickets -> subject }}</td>
                                    <td>{{$tickets -> divisi }}</td>
                                    <td>{{$tickets -> urgent_status }}</td>


                                    <td>

                                        <a href="AdminAccount.tiket.detailtiket.{{$tickets->id_ticket}}" class="btn btn-success">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-3" role="tabpanel">
            <div class="main-card mb-12 card">
                <div class="card-body">
                    <h5 class="card-title">Data Ticket yang sedap dalam progres</h5>
                    <div class="table-responsive">
                        <table class="mb-12 table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Project</th>
                                    <th>Subject</th>
                                    <th>Division</th>
                                    <th>Last Update</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>
                            <?php 
                                    $ticket3 = DB::table('tickets')->where('status_penanganan', '=', 'In-Progress')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                            <tbody>
                                <?php $no = 0; ?>
                                @foreach($ticket3 as $tickets)
                                <?php $no++; ?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{$tickets -> nama }}</td>
                                    <td>{{$tickets -> subject }}</td>
                                    <td>{{$tickets -> divisi }}</td>
                                    <td>{{$tickets -> urgent_status }}</td>


                                    <td>

                                        <a href="AdminAccount.tiket.detailtiket.{{$tickets->id_ticket}}" class="btn btn-success">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach
                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-4" role="tabpanel">
            <div class="main-card mb-12 card">
                <div class="card-body">
                    <h5 class="card-title"> Data Ticket on-hold</h5>
                    <div class="table-responsive">
                        <table class="mb-12 table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Project</th>
                                    <th>Subject</th>
                                    <th>Division</th>
                                    <th>Last Update</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>
                            <?php 
                            $ticket4 = DB::table('tickets')->where('status_penanganan', '=', 'On-Hold')
                            ->join('project', 'tickets.id_project', '=', 'project.id_project')
                            ->select('tickets.*', 'project.nama', 'project.logo')
                            ->get();
                        ?>
                            <tbody>
                                <?php $no = 0; ?>
                                @foreach($ticket4 as $tickets)
                                <?php $no++; ?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{$tickets -> nama }}</td>
                                    <td>{{$tickets -> subject }}</td>
                                    <td>{{$tickets -> divisi }}</td>
                                    <td>{{$tickets -> urgent_status }}</td>


                                    <td>

                                        <a href="AdminAccount.tiket.detailtiket.{{$tickets->id_ticket}}" class="btn btn-success">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach
                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-5" role="tabpanel">
            <div class="main-card mb-12 card">
                <div class="card-body">
                    <h5 class="card-title"> Data Ticket Answered</h5>
                    <div class="table-responsive">
                        <table class="mb-12 table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Project</th>
                                    <th>Subject</th>
                                    <th>Division</th>
                                    <th>Last Update</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>
                            <?php 
                                    $ticket5 = DB::table('tickets')->where('status_penanganan', '=', 'Answered')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                            <tbody>
                                <?php $no = 0; ?>
                                @foreach($ticket5 as $tickets)
                                <?php $no++; ?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{$tickets -> nama }}</td>
                                    <td>{{$tickets -> subject }}</td>
                                    <td>{{$tickets -> divisi }}</td>
                                    <td>{{$tickets -> urgent_status }}</td>


                                    <td>

                                        <a href="AdminAccount.tiket.detailtiket.{{$tickets->id_ticket}}" class="btn btn-success">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach
                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane tabs-animation fade" id="tab-content-6" role="tabpanel">
            <div class="main-card mb-12 card">
                <div class="card-body">
                    <h5 class="card-title">Data Ticket Closed</h5>
                    <div class="table-responsive">
                        <table class="mb-12 table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Project</th>
                                    <th>Subject</th>
                                    <th>Division</th>
                                    <th>Last Update</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>
                             <?php 
                                    $ticket6 = DB::table('tickets')->where('status_penanganan', '=', 'Closed')
                                    ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                    ->select('tickets.*', 'project.nama', 'project.logo')
                                    ->get();
                                ?>
                            <tbody>
                                <?php $no = 0; ?>
                                @foreach($ticket6 as $tickets)
                                <?php $no++; ?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{$tickets -> nama }}</td>
                                    <td>{{$tickets -> subject }}</td>
                                    <td>{{$tickets -> divisi }}</td>
                                    <td>{{$tickets -> urgent_status }}</td>


                                    <td>

                                        <a href="AdminAccount.tiket.detailtiket.{{$tickets->id_ticket}}" class="btn btn-success">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach
                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection