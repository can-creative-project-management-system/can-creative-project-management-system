@extends('layouts/AdminAccount')

@section('title', 'Can Creative Project Management System - Admin Account')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="fa fa-database icon-gradient bg-ripe-malin">
                                        </i>
                                    </div>
                                    <div>Ubah Status Project
                                        <div class="page-title-subheading">.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
                        @if (session('status'))
                        <div class="alert alert-success fade show" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                       
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">Ubah Status</h5>
                                        <form class="" action="{{ route('superadmin.project.simpanstatus', $proyek->id_project)}}" method="POST">
                                           @csrf
                                           @method('patch')
                                             <div class="position-relative form-group"><label for="exampleAddress" class=""></label>
                                                <input type="hidden" name="updated_at" value="{{$proyek->updated_at}}">
                                                <select name="status" class="form-control" id="status">
                                                    <option selected>{{$proyek->status}}</option>
                                                    @if ($proyek->status == "Pra")
                                                        <option value="Produksi">Produksi</option>
                                                        <option value="Pasca">Pasca Produksi</option>
                                                    @elseif ($proyek->status == "Produksi")
                                                        <option value="Pra">Pra Produksi</option>
                                                        <option value="Pasca">Pasca Produksi</option>
                                                    @else
                                                        <option value="Pra">Pra Produksi</option>
                                                        <option value="Produksi">Produksi</option>
                                                    @endif
                                                    
                                                </select>
                                            </div>
                                                
                                                <button class="mt-2 btn btn-primary">Tambah</button>
                                        </form>
                                    </div>
                                </div>
                                
                    @endsection