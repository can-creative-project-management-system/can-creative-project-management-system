@extends('layouts/AdminAccount')

@section('title', 'Can Creative Project Management System - Admin Account')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fa fa-database icon-gradient bg-ripe-malin">
                    </i>
                </div>
                <div>Tambahkan Informasi
                    <div class="page-title-subheading">Isi form dibawah ini untuk menambahkan Informasi terbaru.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
            <div class="main-card mb-3 card">
                <div class="card-body">

                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status')}}
                    </div>
                    @endif
                    <form method="POST" action="AdminAccount.simpan_info" enctype="multipart/form-data">
                        @csrf
                        <div class="position-relative form-group"><label for="exampleAddress" class="">Module</label>
                            <textarea name="module" id="module" class="form-control"></textarea>
                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress" class="">Keterangan</label>
                            <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                        </div>

                        <button class="mt-2 btn btn-primary">Tambah</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection