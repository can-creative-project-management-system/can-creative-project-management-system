@extends('layouts/main')

@section('title', 'Can Creative Project Managament System - Super Admin')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fa fa-database icon-gradient bg-ripe-malin">
                    </i>
                </div>
                <div>Tambahkan Report Project
                    <div class="page-title-subheading">Isi form dibawah ini untuk menambahkan log report terbaru.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    @if (session('pop up'))
                    <div class="alert alert-success">
                        {{ session('pop up')}}
                    </div>
                    @endif
                    <form method="POST" action="/item/image/upload" enctype="multipart/form-data">
                        @csrf
                        <div class="position-relative form-group">
                            <label for="exampleAddress" class="">Nama Project</label>

                            <select class="form-control" name="id_project" id="id_project">
                                <option selected>Pilih Project</option>
                                <?php

                                use Illuminate\Support\Facades\DB;

                                $role = DB::table('project')->get();
                                ?>
                                @foreach($role as $item)
                                <option value="{{$item->id_project}}">{{$item->nama}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress2" class="">Subject</label>
                            <input name="subject" id="subject" placeholder="subject" type="text" class="form-control">
                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress" class="">Change Log</label>
                            <textarea name="changelog" id="changelog" class="form-control"></textarea>
                            @error('changelog')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress2" class="">Link</label>
                            <input name="link" id="link" placeholder="link" type="text" class="form-control">
                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress2" class="">File</label>
                            <input type="file" multiple name="file[]" id="file[]" class="form-control" multiple="true">
                            @error('file')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>


                        <button class="mt-2 btn btn-primary">Tambah</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection