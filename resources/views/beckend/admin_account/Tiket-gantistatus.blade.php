@extends('layouts/AdminAccount')

@section('title', 'Can Creative Project Management System - Admin Account')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="fa fa-database icon-gradient bg-ripe-malin">
                                        </i>
                                    </div>
                                    <div>Ubah Status Penanganan Ticket
                                        <div class="page-title-subheading">.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
                        @if (session('status'))
                        <div class="alert alert-success fade show" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                       
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">Ubah Status Penanganan Ticket</h5>
                                        <form class="" action="{{ route('AdminAccount.tiket.simpanstatus', $Ticket->id_ticket)}}" method="POST">
                                           @csrf
                                           @method('patch')
                                             <div class="position-relative form-group"><label for="exampleAddress" class=""></label>
                                                <input type="hidden" name="updated_at" value="{{$Ticket->updated_at}}">
                                                <select name="status_penanganan" class="form-control" id="status_penanganan">

                                                    <option selected>{{$Ticket->status_penanganan}}</option>
                                                    @if ($Ticket->status_penanganan == "Un-Opened")
                                                        <option value="Open">Open</option>
                                                        <option value="In-Progress">In Progress</option>
                                                        <option value="On-Hold">On Hold</option>
                                                        <option value="Answered">Answered</option>
                                                        <option value="Closed">Closed</option>
                                                    @elseif ($Ticket->status_penanganan == "Open")
                                                        <option value="In-Progress">In Progress</option>
                                                        <option value="On-Hold">On Hold</option>
                                                        <option value="Answered">Answered</option>
                                                        <option value="Closed">Closed</option>
                                                    @elseif ($Ticket->status_penanganan == "In-Progress")
                                                        <option value="On-Hold">On Hold</option>
                                                        <option value="Answered">Answered</option>
                                                        <option value="Closed">Closed</option>
                                                    @elseif ($Ticket->status_penanganan == "On-Hold")
                                                        <option value="Answered">Answered</option>
                                                        <option value="Closed">Closed</option>
                                                    @elseif ($Ticket->status_penanganan == "Answered")
                                                        <option value="Closed">Closed</option>
                                                    @else
                                                    <option value="Closed">Closed</option>
                                                    @endif
                                                    
                                                </select>
                                            </div>
                                                
                                                <button class="mt-2 btn btn-primary">Ganti Status</button>
                                        </form>
                                    </div>
                                </div>
                                
                    @endsection