@extends('layouts/AdminAccount')

@section('title', 'Can Creative Project Management System - Admin Account')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fa fa-database icon-gradient bg-ripe-malin">
                    </i>
                </div>
                <div>Tambah Client Project
                    <div class="page-title-subheading">Tambahkan Client untuk mengakses project,
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">TAMBAH Client</h5>

                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status')}}
                    </div>
                    @endif
                    <form method="POST" action="{{ route('AdminAccount.project.simpan', $proyek->id_project)}}" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <div class="position-relative form-group">
                            <label for="exampleAddress" class="">Nama client</label>
                            <div class="form-row">
                                <select class="form-control" name="id_client" id="id_client">
                                    <option selected>Pilih Client Aktif</option>
                                    <?php

                                    use Illuminate\Support\Facades\DB;

                                    $role = DB::table('users')->where('status', '=', 'Aktif')->where('user_role', '=', '1')->get();
                                    ?>
                                    @foreach($role as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <button class="mt-2 btn btn-primary">Tambah</button>
                        </div>
                    </form>

                </div>

            </div>

        </div>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    </div>
</div>
@endsection