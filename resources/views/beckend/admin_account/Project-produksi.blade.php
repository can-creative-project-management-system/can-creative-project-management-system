@extends('layouts/AdminAccount')

@section('title', 'Can Creative Project Management System - Admin Account')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Data Project Produksi
                    <div class="page-title-subheading">Dibawah ini merupakan list data Project Masa Produksi.
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                <button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fa fa-star"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="main-card mb-12 card">
            <div class="card-body">
                <h5 class="card-title">Table Data Project Produksi</h5>
                <div class="table-responsive">
                    <table class="mb-3 table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Status</th>
                                <th>Aksi</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0; ?>
                            @foreach($proyek as $adm_divisi)
                            <?php $no++; ?>
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{$adm_divisi -> nama }}</td>
                                <td>
                                    @if ($adm_divisi->status == "Pra")
                                    <a href="{{route('AdminAccount.project.gantistatus', $adm_divisi->id_project)}}">
                                        <button class="btn-sm btn-warning">Pra Produksi</button>
                                    </a>
                                @elseif ($adm_divisi->status == "Produksi")
                                    <a href="{{route('AdminAccount.project.gantistatus', $adm_divisi->id_project)}}">
                                        <button class="btn-sm btn-success">Produksi</button>
                                    </a>
                                @else
                                    <a href="{{route('AdminAccount.project.gantistatus', $adm_divisi->id_project)}}">
                                        <button class="btn-sm btn-info">Pasca Produksi</button>
                                    </a>
                                @endif
                                </td>
                                <td>
                                    <a href="AdminAccount.project.detailproject.{{$adm_divisi->id_project}}" class="btn btn-success">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a href="AdminAccount.project.client.{{$adm_divisi->id_project}}" class="btn btn-warning">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <a href="AdminAccount.report.{{$adm_divisi->id_project}}" class="btn btn-info">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                    </a>
                                   
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br><br><br><br><br>
    @endsection