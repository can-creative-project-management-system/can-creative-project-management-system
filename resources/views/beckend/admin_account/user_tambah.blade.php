@extends('layouts/AdminAccount')

@section('title', 'Can Creative Project Management System - Admin Account')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fa fa-database icon-gradient bg-ripe-malin">
                    </i>
                </div>
                <div>Tambah Client
                    <div class="page-title-subheading">Isi form dibawah ini untuk membuat akun client baru.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">TAMBAH CLIENT</h5>

                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status')}}
                    </div>
                    @endif
                    <form method="POST" action="{{ route ('AdminAccount.user.simpan')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6">
                                <input id="user_role" name="user_role" type="hidden" value="1">
                                <div class="position-relative form-group"><label for="exampleEmail11" class="">Email</label>
                                    <input name="email" id="email" placeholder="email" type="email" class="form-control"></div>
                                @error('email')<div class="invalid-feedback">{{$message}}</div> @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group"><label for="examplePassword11" class="">Password</label>
                                    <input name="password" id="examplePassword11" placeholder="password" type="password" class="form-control"></div>
                                @error('password')<div class="invalid-feedback">{{$message}}</div> @enderror
                            </div>
                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress" class="">Nama</label>
                            <input name="name" id="name" placeholder="nama" type="text" class="form-control"></div>
                        @error('name')<div class="invalid-feedback">{{$message}}</div> @enderror
                        <div class="position-relative form-group"><label for="status" class="">Status Client</label><select type="select" id="status" name="status" class="custom-select">
                                <option selected>Status Client</option>
                                <option value="Aktif">Aktif</option>
                                <option value="Nonaktif">Nonaktif</option>
                            </select></div>

                        <button class="mt-2 btn btn-primary">Tambah</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection