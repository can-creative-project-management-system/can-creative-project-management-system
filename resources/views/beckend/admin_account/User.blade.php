@extends('layouts/AdminAccount')

@section('title', 'Can Creative Project Management System - Admin Account')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">

        <div class="tab-content">
            <div class="tab-pane tabs-animation fade show active" id="tab-content-1" role="tabpanel">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{route('AdminAccount.user.tambah')}}">
                            <div class="main-card mb-3 card">
                                <img width="317px" height="210" src="{{url('template/assets/images/2978471-removebg-preview.png')}}" alt="Card image cap" class="card-img-top">
                                <div class="card-body">
                                    <center>
                                        <h5 class="card-title">Buat Akun Client Baru</h5>
                                    </center>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="col-md-4">
                        <a href="{{route('AdminAccount.user.client')}}">
                            <div class="main-card mb-3 card">
                                <img width="317px" height="210" src="{{url('template/assets/images/adminaccount.png')}}" alt="Card image cap" class="card-img-top">
                                <div class="card-body">
                                    <center>
                                        <h5 class="card-title">Client</h5>
                                    </center>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="col-md-4">
                        <a href="{{route('AdminAccount.user.subclient')}}">
                            <div class="main-card mb-3 card">
                                <img width="317px" height="210" src="{{url('template/assets/images/898-removebg-preview.png')}}" alt="Card image cap" class="card-img-top">
                                <div class="card-body">
                                    <center>
                                        <h2 class="card-title">Sub-Client</h2>
                                    </center>
                                </div>
                            </div>
                        </a>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection