@extends('layouts/AdminAccount')

@section('title', 'Project Managament System - Admin Account')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Data Admin Info
                    <div class="page-title-subheading">Tabel dibawah ini merupakan list data Admin Info.
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-lg-12">
        <div class="main-card mb-12 card">
            <div class="card-body">
                <h5 class="card-title">Table Data Admin Info</h5>
                <div class="table-responsive">
                    <table class="mb-3 table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Kontak</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0; ?>
                            @foreach($admin as $adm_divisi)
                            <?php $no++; ?>
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{$adm_divisi -> name }}</td>
                                <td>{{$adm_divisi -> email}}</td>
                                <td>{{$adm_divisi -> tlp}}</td>
                                <td>
                                    @if ($adm_divisi->status == "Nonaktif")
                                        <a href="{{route('AdminAccount.admindivisi.gantistatus', $adm_divisi->id)}}">
                                            <button class="btn-sm btn-danger">Nonaktif</button>
                                        </a>
                                    @else
                                        <a href="{{route('AdminAccount.admindivisi.gantistatus', $adm_divisi->id)}}">
                                            <button class="btn-sm btn-info">Aktif</button>
                                        </a>
                                    @endif
                                </td>

                                <td>
                                    <a href="{{route('AdminAccount.admin.detail', $adm_divisi->id)}}" class="btn btn-success">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                   
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br><br><br><br><br>
    @endsection