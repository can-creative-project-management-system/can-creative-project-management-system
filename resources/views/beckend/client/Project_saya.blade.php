@extends('layouts/Client')

@section('title', 'Project Managament System - Client')

@section ('container')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="tab-content">
            <div class="tab-pane tabs-animation fade show active" id="tab-content-1" role="tabpanel">
                <div class="row"> 
                    <?php
                    use Illuminate\Support\Facades\Session;
                    use Illuminate\Support\Facades\DB;
                    $krisan = DB::table('project')->where('id_client', Session::get('id'))->get();
                    ?>

                @foreach($krisan as $myproyek)
                <div class="col-md-4">
                    
                        <div class="main-card mb-3 card">
                            <img width="317px" height="210px" src="{{url('logo/'.$myproyek->logo)}}" alt="Card image cap" class="card-img-top">
                            <div class="card-body">

                                <center>
                                    <h5 class="card-title">{{ $myproyek -> nama}}</h5>
                                    <a href="client.project.detailproject.{{$myproyek->id_project}}" class="btn btn-success">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a href="client.project.client.{{$myproyek->id_project}}" class="btn btn-warning">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <a href="client.report.{{$myproyek->id_project}}" class="btn btn-info">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                    </a>
                                    <a href="client.ticket.tambah" class="btn btn-alternate">
                                        <i class="fa fa-pen" aria-hidden="true"></i>
                                    </a>
                                </center>
                            </div>
                        </div>
                   

                </div>
                @endforeach
                </div>
            </div>

        </div>
    </div>

</div>
<!-- <div class="container">
    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-1" role="tabpanel">
           
            <div class="row">
                
            </div>
        </div>
    </div>

</div> -->

@endsection