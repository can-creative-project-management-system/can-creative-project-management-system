@extends('layouts/Client')

@section('title', 'Project Managament System - Client')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Data Tiket
                    <div class="page-title-subheading">Dibawah ini merupakan list data detail ticket.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="main-card mb-12 card">
            <div class="card">
                <div class="card-body">
                    <div class="package text-left bg-white">
                        <table class="table" border="1" style="border-color: white">
                            <tr>
                                <th style="color:#696969">Nama Project</th>
                                <td style="font-size: 20px;  color: #000000;">{{ $ticket->nama }}</td>
                            </tr>
                            <tr>
                                <th style="color:#696969">Nama Client</th>
                                <td style="font-size: 20px; color: #000000;">{{ $ticket->name }}</td>
                            </tr>

                            <tr>
                                <th style="color:#696969">Nama SubClient</th>
                                <td style="font-size: 20px; color: #000000;">{{ $ticket->id_subclient }}</td>
                            </tr>

                            <tr>
                                <th style="color:#696969">Subject</th>
                                <td style="font-size: 20px; color: #000000;">{{ $ticket->subject }}</td>
                            </tr>

                            <tr>
                                <th style="color:#696969">Division </th>
                                <td style="font-size: 20px; color: #000000;">{{ $ticket->divisi }}</td>
                            </tr>
                            <tr>
                                <th style="color:#696969">Urgent Status</th>
                                <td style="font-size: 20px; color: #000000;">{{ $ticket->urgent_status }}</td>
                            </tr>

                            <tr>
                                <th style="color:#696969">Detail</th>
                                <td style="font-size: 20px;color: #000000;">{{ $ticket->detail_ticket }}</td>
                            </tr>
                            <tr>
                                <th style="color:#696969">Status Penanganan</th>
                                <td style="font-size: 20px;color: #000000;">{{ $ticket->status_penanganan }}</td>
                            </tr>
                            <tr>
                                <th style="color:#696969">File</th>
                                <td style="font-size: 20px;color: #000000;">{{ $ticket->file}}</td>
                            </tr>
                        </table>

                        <a href="{{route('client.ticket')}}" class="btn btn-primary btn-sm">Back</a>
                        <button class="btn btn-primary" id="btn-komentar-utama"><i class="fa fa-comment"></i> Komentar</button>



                        <form action="" style="margin-top: 10px;display:none;margin-bottom:10px;" id="komentar-utama" method="POST">
                            @csrf
                            <input type="hidden" name="id_ticket" id="" value="{{$ticket->id_ticket}}">
                            <input type="hidden" name="id_project" id="" value="{{$ticket->id_project}}">
                            <input type="hidden" name="id_user" id="">
                            <input type="hidden" name="id_admin">
                            <input type="hidden" name="name" value="{{$ticket->name}}">
                            <input type="hidden" name="parent" value="0">
                            <textarea style="margin-top: 10px;" name="komentar" id="komentar-utama" cols="30" rows="4" class="form-control"></textarea>
                            <input style="margin-top: 10px;" type="submit" class="btn btn-primary" value="Kirim"></input>
                        </form>

                        <br><br>
                        <h3>Komentar</h3>
                        <ul class="list-unstyled activity-list mt-3">
                            <?php

                            use Illuminate\Support\Facades\Session;
                            use Illuminate\Support\Facades\DB;

                            $arrURL = explode("http://", url()->current());
                            $arr2URL = $arrURL[1];
                            $arr3URL = explode("/", $arr2URL);
                            $arrFix = explode(".",$arr3URL[1]);
                            $komentar = DB::table('komentars')
                                // ->leftJoin('posts', 'users.id', '=', 'posts.user_id')

                                ->leftjoin('users', 'komentars.id_user', '=', 'users.id')
                                ->leftjoin('admin', 'komentars.id_admin', '=', 'admin.id')
                                ->join('tickets', 'komentars.id_ticket', '=', 'tickets.id_ticket')
                                // ->select('komentars.*', 'users.name', 'admin.name')->groupBy(
                                //     'komentars.id_user',
                                //     'komentars.id_admin',
                                //     'komentars.id',
                                //     'komentars.id_admin',
                                //     'komentars.id_project',
                                //     'komentars.id_ticket',
                                //     'users.name',
                                //     'admin.name',
                                //     'komentars.komentar',
                                //     'komentars.parent',
                                //     'komentars.created_at',
                                //     'komentars.updated_at'
                                // )
                                ->select('komentars.*',  'admin.name AS aname', 'users.name AS uname')->distinct()
                                ->where('komentars.id_ticket', $arrFix[2])
                                ->get()



                            ?>
                            @foreach($komentar as $komentar)
                            <li>
                                <p>
                                    @if ($komentar->id_admin != Null)
                                    <a>{{$komentar->aname }}</a>
                                    @else
                                    <a>{{$komentar->uname }}</a>
                                    @endif
                                </p>
                                <p>{{$komentar->komentar}}</p>

                                <p> <span class="timestamp">{{$komentar->created_at}}</span>
                                </p>


                            </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>

</div>
</form>
</div>
</div>
</div>

@endsection