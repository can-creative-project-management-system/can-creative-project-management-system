@extends('layouts/Client')

@section('title', 'Project Managament System - Client')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Client Dashboard
                    <div class="page-title-subheading">Halaman ini merupakan halaman dashboard Client.
                    </div>
                </div>
                
            </div>
            <div class="page-title-actions">
                
                <div class="d-inline-block dropdown">
                    <a href="Client.project.tambahsubclient">
                    <button type="button" aria-expanded="false" class="btn-shadow btn btn-info">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-business-time fa-w-20"></i>
                        </span>
                        Daftarkan Sub - Client
                    </button>
                </a>
                </div> 
            </div>    
        </div>
    </div>
    
    <?php 
    $proyek = DB::table('project')->where('id_client', '=', Session::get('id'))->get();
    $subclient = DB::table('users')
            ->where('id_client', '=', Session::get('id'))
            ->where('user_role', '=', '2')
            ->get();
    $ticket = DB::table('tickets')
            ->where('id_client', '=', Session::get('id'))
            ->get();

    $proyekCount = $proyek->count(); 
    $subclientCount = $subclient->count(); 
    $ticketcount = $ticket->count();    
    ?>     

    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Projek Saya</div>
                        <div class="widget-subheading">Total Projek Aktif</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span>{{$proyekCount}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-arielle-smile">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Sub - Clients</div>
                        <div class="widget-subheading">Total Sub - Clients </div>
                    </div>
                    <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span>{{$subclientCount}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-happy-itmeo">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Ticket</div>
                        <div class="widget-subheading">Total Ticket</div>
                    </div>
                    <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span>{{$ticketcount}}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-header">List tiket yang belum selesai
                </div>
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th>Project</th>
                            <th class="text-center">Subject</th>
                            <th class="text-center">Division</th>
                            <th class="text-center">Urgent Status</th>
                            <th class="text-center">Last Update</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $krisan = DB::table('tickets')
                                ->where('tickets.id_client', '=', Session::get('id'))
                                ->join('project', 'tickets.id_project', '=', 'project.id_project')
                                ->select('tickets.*', 'project.nama', 'project.logo')
                                ->where('status_penanganan', '!=', 'Closed')
                                ->get();
                            ?>
                            @foreach ($krisan as $no => $fb)
                            <tr>
                                <td class="text-center text-muted">{{$no+1}}</td>
                                <td>
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left mr-3">
                                                <div class="widget-content-left">
                                                <img width="40" class="rounded-circle" src="{{url('logo/'.$fb->logo)}}" alt="">
                                                </div>
                                            </div>
                                            <div class="widget-content-left flex2">
                                                <div class="widget-heading">{{$fb->nama}}</div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">{{$fb->subject}}</td>
                                <td class="text-center">{{$fb->divisi}}</td>
                                <td class="text-center">
                                    <div class="badge">
                                        @if ($fb->urgent_status == "High")
                                            <a href="">
                                                <button class="btn-sm btn-danger" disabled>High</button>
                                            </a>
                                        @elseif ($fb->urgent_status == "Medium")
                                            <a href="">
                                                <button class="btn-sm btn-warning" disabled>Medium</button>
                                            </a>
                                        @else
                                        <a href="">
                                            <button class="btn-sm btn-info" disabled>Low</button>
                                        </a>
                                        @endif
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="badge">
                                        @if ($fb->status_penanganan == "Un-Opened")
                                            <a href="">
                                                <button class="btn-sm btn-warning" disabled>Un-Opened</button>
                                            </a>
                                        @elseif ($fb->status_penanganan == "Open")
                                            <a href="">
                                                <button class="btn-sm btn-info" disabled>Open</button>
                                            </a>
                                        @elseif ($fb->status_penanganan == "In-Progress")
                                            <a href="">
                                                <button class="btn-sm btn-alt" disabled>In-Progress</button>
                                            </a>
                                        @elseif ($fb->status_penanganan == "On-Hold")
                                            <a href="">
                                                <button class="btn-sm btn-danger" disabled>On-Hold</button>
                                            </a>
                                        @else
                                        <a href="">
                                            <button class="btn-sm btn-success" disabled>Answered</button>
                                        </a>
                                        @endif
                                    </div>
                                </td>
                                <td><br></td>
                                <td class="text-center">
                                <a href="superadmin.tiket.detailtiket.{{$fb->id_ticket}}">
                                    <button type="button" id="PopoverCustomT-1" class="btn btn-primary btn-sm">Details</button>
                                </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection