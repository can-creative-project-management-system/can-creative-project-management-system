@extends('layouts/Client')

@section('title', 'Project Managament System - Client')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-help1 icon-gradient bg-night-fade">
                                        </i>
                                    </div>
                                    <div>Data Informasi
                                        <div class="page-title-subheading">Tabel dibawah ini merupakan list projek dalam masa Produksi.
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>           
                        <div class="row">
                            <div class="col-md-6">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <div class="card-title">Media</div>
                                        <center>
                                            <a href="https://www.instagram.com/cancreative">
                                                <img width="65px" src="{{url('template/assets/images/2227-removebg-preview.png')}}" alt="Card image cap" class="">
                                            </a>
                                            <a href="https://can.co.id/#">
                                                <img width="50px" src="{{url('template/assets/images/iconfinder_Google_1298745.png')}}" alt="Card image cap" class="">
                                            </a>
                                            <a href="https://twitter.com/can_coid">
                                                <img width="50px" src="{{url('template/assets/images/twitter.png')}}" alt="Card image cap" class="">
                                            </a>
                                            <a href="https://www.facebook.com/can.co.id/">
                                                <img width="50px" src="{{url('template/assets/images/facebook.png')}}" alt="Card image cap" class="">
                                            </a>
                                        </center>
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <div class="card-title">Alamat Kantor Can Creative</div>
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.3316688619793!2d110.40102551477311!3d-6.9701420949645145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708b12b5694f0f%3A0x72210ee40fee75ce!2sCAN%20Creative%20%7C%20Jasa%20Pembuatan%20Web%20dan%20Aplikasi%20Mobile%20Android%20IOS!5e0!3m2!1sid!2sid!4v1609732071693!5m2!1sid!2sid" width="100%" height="200px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <div class="table-hover">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Module</th>
                                                    <th>Keterangan</th>
                                                   
                                                </tr>
                                            </thead>
                                           
                                            
                                            <tbody>
                                                @foreach ($info as $no => $fb)
                                                <tr>
                                                    <td>{{$fb->module}}</td>
                                                    <td>{{$fb->keterangan}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <!-- /.container-fluid -->
                        
                            <!-- Bootstrap core JavaScript-->
                            <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
                            <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                        
                            <!-- Core plugin JavaScript-->
                            <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
                        
                            <!-- Custom scripts for all pages-->
                            <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
                        
                            <!-- Page level plugins -->
                            <script src="{{url('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
                            <script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        
                            <!-- Page level custom scripts -->
                            <script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
                            <br><br><br><br><br><br><br><br>
                    @endsection