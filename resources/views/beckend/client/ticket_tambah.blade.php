@extends('layouts/Client')

@section('title', 'Can Creative Project Management System - Client')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fa fa-database icon-gradient bg-ripe-malin">
                    </i>
                </div>
                <div>Buat Ticket Project
                    <div class="page-title-subheading">Isi form dibawah ini untuk menambahkan ticket terbaru.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
            <div class="main-card mb-3 card">
                <div class="card-body">

                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status')}}
                    </div>
                    @endif
                    <form method="POST" action="client.ticket.simpan" enctype="multipart/form-data">
                        @csrf
                        <input id="status_penanganan" name="status_penanganan" type="hidden" value="Un-Opened">
                        <input id="id_client" name="id_client" type="hidden">
                        <div class="position-relative form-group">
                            <label for="exampleAddress" class="">Nama Project</label>

                            <select class="form-control" name="id_project" id="id_project">
                                <option selected>Pilih Project</option>
                                <?php

                                use Illuminate\Support\Facades\DB;

                                $role = DB::table('project')->get();
                                ?>
                                @foreach($role as $item)
                                <option value="{{$item->id_project}}">{{$item->nama}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress2" class="">Subject</label>
                            <input name="subject" id="subject" placeholder="subject" type="text" class="form-control">
                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress" class="">Detail</label>
                            <textarea name="detail" id="detail" class="form-control"></textarea>
                            @error('detail')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>

                        <div class="position-relative form-group"><label for="Divisi" class="">Divisi Tujuan</label><select type="select" id="divisi" name="divisi" class="custom-select">
                                <option selected></option>
                                <option value="support">Divisi Support</option>
                                <option value="billing">Divisi Billing</option>
                                <option value="info">Divisi Info</option>
                            </select>
                        </div>
                        <div class="position-relative form-group"><label for="Urgent Status" class="">Urgent Status</label><select type="select" id="urgent_status" name="urgent_status" class="custom-select">
                                <option selected></option>
                                <option value="High">High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                            </select>
                        </div>
                        <div class="position-relative form-group"><label for="exampleAddress2" class="">File</label>
                            <input name="file[]" id="file" placeholder="file" type="file" class="form-control">
                            @error('file')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>

                        <button class="mt-2 btn btn-primary">Tambah</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection