@extends('layouts/AdminProduksi')

@section('title', 'Project Managament System - Admin Produksi')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Detail Data Ticket
                                        <div class="page-title-subheading">Tabel dibawah ini merupakan detail Ticket.
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>           
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Detail Data Ticket</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table" id="dataTable" width="100%">
                                            @foreach($project as $fb)
                                            <tr>
                                                <th style="">Nama Project</th>
                                                <th> : </th>
                                                <td style="">{{ $fb->nama}}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Subject</th>
                                                <th> : </th>
                                                <td style="">{{ $fb->subject }}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Detail</th>
                                                <th> : </th>
                                                <td style="">{{ $fb->detail_ticket}}</td>
                                            </tr>

                                            <tr>
                                                <th style="">Divisi Tujuan</th>
                                                <th> : </th>
                                                <td style="">{{ $fb->divisi}}</td>
                                            </tr>

                                            <tr>
                                                <th style="">Urgent Status</th>
                                                <th> : </th>
                                                <td style="">
                                                    @if ($fb->urgent_status == "High")
                                                        <a href="">
                                                            <button class="btn-sm btn-danger">High</button>
                                                        </a>
                                                    @elseif ($fb->urgent_status == "Medium")
                                                        <a href="">
                                                            <button class="btn-sm btn-warning">Medium</button>
                                                        </a>
                                                    @else
                                                        <a href="">
                                                            <button class="btn-sm btn-info">Low</button>
                                                        </a>
                                                    @endif
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="">Penanganan</th>
                                                <th> : </th>
                                                <td style="">
                                                    @if ($fb->status_penanganan == "Un-Opened")
                                                        <a href="{{route('AdminProduksi.tiket.gantistatus', $fb->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-warning">Unopened</button>
                                                        </a>
                                                    @elseif ($fb->status_penanganan == "Open")
                                                        <a href="{{route('AdminProduksi.tiket.gantistatus', $fb->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-info">Open</button>
                                                        </a>
                                                    @elseif ($fb->status_penanganan == "In-Progress")
                                                        <a href="{{route('AdminProduksi.tiket.gantistatus', $fb->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-alternate">In Progress</button>
                                                        </a>
                                                    @elseif ($fb->status_penanganan == "On-Hold")
                                                        <a href="{{route('AdminProduksi.tiket.gantistatus', $fb->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-danger">On Hold</button>
                                                        </a>
                                                    @elseif ($fb->status_penanganan == "Answered")
                                                        <a href="{{route('AdminProduksi.tiket.gantistatus', $fb->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-primary">Answered</button>
                                                        </a>
                                                    @else
                                                        <a href="{{route('AdminProduksi.tiket.gantistatus', $fb->id_ticket)}}">
                                                            <button class="mb-2 mr-2 btn-transition btn btn-outline-success">Closed</button>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="">Waktu Dibuat</th>
                                                <th> : </th>
                                                <td style="">{{ $fb->created_at}}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Terakhir Di-Update</th>
                                                <th> : </th>
                                                <td style="">{{ $fb->updated_at}}</td>
                                            </tr>
                                            
                                        </table>
                                        <tr>
                                            <th><a href="{{url('/AdminProduksi/jawab',$fb->id_ticket)}}">
                                                <button type="button" class="btn-shadow btn btn-info">
                                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                                        <i class="fa fa-business-time fa-w-20"></i>
                                                    </span>
                                                    Balas
                                                </button>
                                                </a>
                                            </th>
                                        </tr>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Jawaban Ticket</h6>
                                </div>
                                @if ($fb->jawaban == "0")
                                    <div class="card-body">
                                        <h6>
                                            Belum Ada Jawaban
                                        </h6>
                                    </div>
                                @else
                                <div class="card-body">
                                    <div class="">
                                        <th>
                                            {{ $fb->jawaban}}
                                        </th>
                                        <br><br><br>
                                    </div>
                                    <div>
                                        <th>
                                            <b>Best Regard,</b>
                                        </th>
                                        <br>
                                        <th>
                                            <b>Emma</b>
                                        </th>
                                        <br>
                                        <th>
                                            <b>Account Division</b>
                                        </th>
                                    </div>
                                </div>
                                @endif
                            </div>
                           @endforeach
                        </div>
                        <!-- /.container-fluid -->
                        
                            <!-- Bootstrap core JavaScript-->
                            <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
                            <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                        
                            <!-- Core plugin JavaScript-->
                            <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
                        
                            <!-- Custom scripts for all pages-->
                            <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
                        
                            <!-- Page level plugins -->
                            <script src="{{url('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
                            <script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        
                            <!-- Page level custom scripts -->
                            <script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
                            <br><br><br><br><br><br><br><br>
                    @endsection
