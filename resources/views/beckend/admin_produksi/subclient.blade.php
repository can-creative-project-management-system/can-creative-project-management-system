@extends('layouts/AdminProduksi')

@section('title', 'Project Managament System - Admin Produksi')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Data Sub-Client
                    <div class="page-title-subheading">Dibawah ini merupakan list data Sub-Client.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="main-card mb-12 card">
            <div class="card-body">
                <h5 class="card-title">Table Data Sub - Client</h5>
                <div class="table-responsive">
                    <table class="mb-3 table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Status</th>

                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0; ?>
                            @foreach($client as $adm_divisi)
                            <?php $no++; ?>
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{$adm_divisi -> name }}</td>
                                <td>{{$adm_divisi -> email}}</td>
                                <td>

                                    @if($adm_divisi == 'Nonaktif')
                                    <a href="" class="btn btn-warning">
                                        Aktif
                                    </a>
                                    @else
                                    <a href="" class="btn btn-info">
                                        Aktif
                                    </a>
                                    @endif
                                </td>


                                <td>
                                    <a href="{{route('AdminProduksi.user.detail', $adm_divisi->id)}}" class="btn btn-success">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>

                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br><br><br><br><br>
    @endsection