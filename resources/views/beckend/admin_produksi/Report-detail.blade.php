@extends('layouts/AdminProduksi')

@section('title', 'Project Managament System - Admin Produksi')

@section ('container')

                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Detail Report
                                        <div class="page-title-subheading">
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                        </div>           
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Detail Data Report</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table" id="dataTable" width="100%" cellspacing="0">
                                            
                                            <tr>
                                                <th style="">Nama Project</th>
                                                <th>:</th>
                                                <td style="">{{ $DataAdmin->nama }}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Detail</th>
                                                <th>:</th>
                                                <td style="">{{ $DataAdmin->subject }}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Change Log</th>
                                                <th>:</th>
                                                <td style="">{{ $DataAdmin->changelog }}</td>
                                            </tr>

                                            <tr>
                                                <th style="">Link</th>
                                                <th>:</th>
                                                <td style="">
                                                    <a href="{{ $DataAdmin->link}}" class="btn btn-info">
                                                       Tekan Untuk Membuka Link Progress Report
                                                    </a>
                                                    </td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                    <div>
                                        <?php $file = DB::table('file')->where('id_report',$DataAdmin->id_report)->get(); ?>
                                        <div class="row">
                                            @foreach ($file as $no => $file)
                                            <div class="col-lg-2">
                                                <a href="/AdminProduksi/download/{{$file->file}}">
                                                    <button type="button" aria-expanded="false" class="btn-shadow btn btn-success">
                                                        <span class="btn-icon-wrapper pr-2 opacity-7">
                                                            <i class="fa fa-business-time fa-w-20"></i>
                                                        </span>
                                                        Download File <th>{{$no+1}}</th>
                                                    </button>
                                                </a>
                                            </div>
                                            
                                            @endforeach
                                        </div>
                                        <br>
                                        <div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <!-- /.container-fluid -->
                        
                            <!-- Bootstrap core JavaScript-->
                            <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
                            <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                        
                            <!-- Core plugin JavaScript-->
                            <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
                        
                            <!-- Custom scripts for all pages-->
                            <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
                        
                            <!-- Page level plugins -->
                            <script src="{{url('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
                            <script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        
                            <!-- Page level custom scripts -->
                            <script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
                            <br><br><br><br><br><br><br><br>
                    @endsection