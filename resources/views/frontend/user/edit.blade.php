@extends('layouts/Client')

@section('title', 'Data Client - Client')

@section ('container')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-graph text-success">
                    </i>
                </div>
                <div>Edit Profil


                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">EDIT PROFIL </h5>

                    <form method="POST" action="{{ route('profil.edit1', $users->id)}}" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="position-relative form-group"><label for="exampleEmail11" class="">Nama</label>
                                    <input name="name" id="name" placeholder="nama" type="name" class="form-control">
                                </div>
                                @error('email')<div class="invalid-feedback">{{$message}}</div> @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group"><label for="exampleEmail11" class="">Email</label>
                                    <input name="email" id="email" placeholder="email" type="email" class="form-control">
                                </div>
                                @error('email')<div class="invalid-feedback">{{$message}}</div> @enderror
                            </div>

                            <div class="col-md-6">
                                <div class="position-relative form-group"><label for="exampleAddress2" class="">Kontak</label>
                                    <input name="tlp" id="kontak" placeholder="kontak" type="text" class="form-control">
                                </div>
                                @error('kontak')<div class="invalid-feedback">{{$message}}</div> @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group"><label for="exampleAddress2" class="">Alamat</label>
                                    <input name="alamat" id="alamat" placeholder="alamat" type="text" class="form-control">
                                </div>
                                @error('alamat')<div class="invalid-feedback">{{$message}}</div> @enderror
                            </div>

                            <div class="col-md-6">
                                <button class="mt-2 btn btn-primary">Edit</button>
                            </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection