<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>

<body>
    <p>Hello, {{ $email }} </p>
    <p>

        We wanted to let you know that your GitHub password was reset.</p>

    <p> If you did not perform this action, you can recover access by entering </p> {{ $email }}</p> <p> into the form at https://localhost:8000/user/forgotPw </p>


    <p> Please do not reply to this email with your password. We will never ask for your password, and we strongly discourage you from sharing it with anyone.


    </p>



    <p>Thanks,</p>
    <p>Admin Account</p>
</body>

</html>