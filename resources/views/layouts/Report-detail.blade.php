@extends('layouts/main')

@section('title', 'Project Managament System - Super Admin')

@section ('container')
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Detail Report
                                        <div class="page-title-subheading">
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                        </div>           
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Detail Data Report</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table" id="dataTable" width="100%" cellspacing="0">
                                            @foreach($report as $DataAdmin)
                                            {{-- <tr>
                                                <td style="font-size: 20px;color: #000000; "><img width="300px" src="{{ url('logo/'.$DataAdmin->logo) }}"></td>
                                            </tr> --}}
                                            <tr>
                                                <th style="">Nama Project</th>
                                                <td style="">{{ $DataAdmin->id_project }}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Detail</th>
                                                <td style="">{{ $DataAdmin->subject }}</td>
                                            </tr>
                                            <tr>
                                                <th style="">Mou</th>
                                                <td style="">{{ $DataAdmin->changelog }}</td>
                                            </tr>

                                            <tr>
                                                <th style="">Due Project</th>
                                                <td style="">{{ $DataAdmin->link}}</td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <!-- /.container-fluid -->
                        
                            <!-- Bootstrap core JavaScript-->
                            <script src="{{url('assets/vendor/jquery/jquery.min.js')}}"></script>
                            <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                        
                            <!-- Core plugin JavaScript-->
                            <script src="{{url('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
                        
                            <!-- Custom scripts for all pages-->
                            <script src="{{url('assets/js/sb-admin-2.min.js')}}"></script>
                        
                            <!-- Page level plugins -->
                            <script src="{{url('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
                            <script src="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        
                            <!-- Page level custom scripts -->
                            <script src="{{url('assets/js/demo/datatables-demo.js')}}"></script>
                            <br><br><br><br><br><br><br><br>
                    @endsection