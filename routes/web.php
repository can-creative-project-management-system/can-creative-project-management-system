<?php

use Illuminate\Support\Facades\Route;

//SuperAdmin 

//SuperAdmin - dashboard
Route::get('superadmin', 'SuperAdmin\superadmin@index')->name('superadmin');

//SuperAdmin - halaman admin divisi
Route::get('superadmin.admindivisi', 'SuperAdmin\AdminDivisi@index')->name('superadmin.admindivisi');
Route::get('superadmin.admindivisi.adminaccount', 'SuperAdmin\AdminDivisi@adminaccount')->name('superadmin.admindivisi.adminaccount');
Route::get('superadmin.admindivisi.admininfo', 'SuperAdmin\AdminDivisi@admininfo')->name('superadmin.admindivisi.admininfo');
Route::get('superadmin.admindivisi.adminproduksi', 'SuperAdmin\AdminDivisi@adminproduksi')->name('superadmin.admindivisi.adminproduksi');

//SuperAdmin - Tambah Admin Divisi 
Route::get('superadmin.tambahadmindivisi', 'SuperAdmin\AdminDivisi@tambah')->name('superadmin.tambahadmindivisi');
Route::post('superadmin.tambahadmindivisi', 'SuperAdmin\AdminDivisi@simpan')->name('prosestambah_admindivisi');
Route::get('superadmin.tambahadmin_account', 'SuperAdmin\AdminDivisi@tambah_adm_account')->name('superadmin.tambahadmin_account');
Route::post('superadmin.tambahadmin_account', 'SuperAdmin\AdminDivisi@simpan_adm_account')->name('prosesTambahadmin_account');
Route::get('superadmin.tambahadmin_info', 'SuperAdmin\AdminDivisi@tambah_adm_info')->name('superadmin.tambahadmin_info');
Route::post('superadmin.tambahadmin_info', 'SuperAdmin\AdminDivisi@simpan_adm_info')->name('prosestambahadmin_info');

//SuperAdmin - detail admin
Route::get('superadmin.admin.detail.{id}', 'SuperAdmin\AdminDivisi@detail')->name('superadmin.admin.detail');

//SuperAdmin - ganti status admin
Route::get('superadmin.admin.{id}.gantistatus', 'SuperAdmin\AdminDivisi@gantistatus')->name('superadmin.admin.gantistatus');
Route::patch('superadmin.admin.simpanstatus.{id}', 'SuperAdmin\AdminDivisi@simpanstatus')->name('superadmin.admin.simpanstatus');

//SuperAdmin - project
Route::get('superadmin.project', 'SuperAdmin\Project@index')->name('superadmin.project');
Route::get('superadmin.project.praproduksi', 'SuperAdmin\Project@praproduksi')->name('superadmin.project.praproduksi');
Route::get('superadmin.project.produksi', 'SuperAdmin\Project@produksi')->name('superadmin.project.produksi');
Route::get('superadmin.project.pascaproduksi', 'SuperAdmin\Project@pascaproduksi')->name('superadmin.project.pascaproduksi');

//SuperAdmin - Tambah Project
Route::get('superadmin.tambah_project', 'SuperAdmin\Project@tambah_project')->name('superadmin.tambah_project');
Route::post('superadmin.simpan_project', 'SuperAdmin\Project@simpan_project')->name('superadmin.simpan_project');

//SuperAdmin - Tambah client kedalam project
Route::get('superadmin.project.client.{id_project}', 'SuperAdmin\Project@client')->name('superadmin.project.client');
Route::patch('superadmin.project.simpan.{id_project}', 'SuperAdmin\Project@update')->name('superadmin.project.simpan');

//SuperAdmin - Ganti Status Project
Route::get('superadmin.project.{id_project}.gantistatus', 'SuperAdmin\Project@gantistatus')->name('superadmin.project.gantistatus');
Route::patch('superadmin.project.simpanstatus.{id_project}', 'SuperAdmin\Project@simpanstatus')->name('superadmin.project.simpanstatus');

//SuperAdmin - Detail Project
Route::get('superadmin.project.detailproject.{id_project}', 'SuperAdmin\Project@detail')->name('superadmin.project.detailproject');

//Report
Route::get('superadmin.report.{id_project}', 'SuperAdmin\Report@index')->name('superadmin.report');

//SuperAdmin - tambah report
Route::get('superadmin.tambah_report', 'SuperAdmin\Report@tambah')->name('superadmin.tambah_report');
Route::post('/item/image/upload', 'SuperAdmin\Report@simpan')->name('superadmin.report.simpan');

//SuperAdmin - Delete report
// Route::get('/item/image/delete', 'SuperAdmin\Report@removeUpload');
Route::delete('superadmin.report.delete.{id_report}', 'SuperAdmin\Report@delete')->name('superadmin.report.delete');

//SuperAdmin - Edit report
// Route::get('superadmin/report/edit{id_report}', 'SuperAdmin\Report@edit')->name('superadmin/report/edit');
// Route::patch('superadmin/report/simpanedit/{id_report}', 'SuperAdmin\Report@simpanedit')->name('superadmin.report.simpanedit');

//SuperAdmin - Detail Report
Route::get('superadmin/report/detailreport/{id_report}', 'SuperAdmin\Report@detail')->name('superadmin.report.detailreport');

//SuperAdmin - Deownload file report
Route::get('/file/download/{file}', 'SuperAdmin\Report@download');

//SuperAdmin - Ticket
Route::get('superadmin.tiket', 'SuperAdmin\Ticket@index')->name('superadmin.tiket');
Route::get('superadmin.tiket.detailtiket.{id_ticket}', 'SuperAdmin\Ticket@detail')->name('superadmin.tiket.detailtiket');
Route::get('superadmin.tiket.{id_ticket}.gantistatus', 'SuperAdmin\Ticket@gantistatus')->name('superadmin.tiket.gantistatus');
Route::patch('superadmin.tiket.simpanstatus.{id_ticket}', 'SuperAdmin\Ticket@simpanstatus')->name('superadmin.tiket.simpanstatus');
Route::get('superadmin.tiket.jawabtiket.{id_ticket}', 'SuperAdmin\Ticket@jawab')->name('superadmin.tiket.jawabtiket');
Route::patch('superadmin.tiket.simpanjawab.{id_ticket}', 'SuperAdmin\Ticket@simpanjawab')->name('superadmin.tiket.simpanjawab');

//SuperAdmin - Users
Route::get('superadmin.user', 'SuperAdmin\User@index')->name('superadmin.user');
Route::get('superadmin.user.tambah', 'SuperAdmin\User@tambah')->name('superadmin.user.tambah');
Route::post('superadmin.user.simpan', 'SuperAdmin\User@simpan')->name('superadmin.user.simpan');
Route::get('superadmin.user.client', 'SuperAdmin\User@client')->name('superadmin.user.client');
Route::get('superadmin.user.subclient', 'SuperAdmin\User@subclient')->name('superadmin.user.subclient');

Route::get('tambah.subclient', 'SuperAdmin\User@tambahsubclient')->name('tambah.subclient');

Route::get('superadmin.user.{id}.gantistatus', 'SuperAdmin\User@gantistatus')->name('superadmin.user.gantistatus');
Route::patch('superadmin.user.simpanstatus.{id}', 'SuperAdmin\User@simpanstatus')->name('superadmin.user.simpanstatus');
Route::get('superadmin.user.detail.{id}', 'SuperAdmin\User@detail')->name('superadmin.user.detail');
Route::get('superadmin.user.{id}.gantistatusub', 'SuperAdmin\User@gantistatusub')->name('superadmin.user.gantistatusub');
Route::patch('superadmin.user.simpanstatusub.{id}', 'SuperAdmin\User@simpanstatusub')->name('superadmin.user.simpanstatusub');

//super Admin - Info
Route::get('superadmin.informasi', 'SuperAdmin\Project@info')->name('superadmin.informasi');
Route::get('superadmin.tambah_info', 'SuperAdmin\Project@tambah_info')->name('superadmin.tambah_info');
Route::post('superadmin.simpan_info', 'SuperAdmin\Project@simpan_info')->name('superadmin.simpan_info');
Route::get('superadmin.info.ubah.{id}', 'SuperAdmin\Project@ubahstatus')->name('superadmin.info.ubah');
Route::patch('superadmin.info.simpan.{id}', 'SuperAdmin\Project@savestatus')->name('superadmin.info.simpan');
Route::get('superadmin.info.edit.{id}', 'SuperAdmin\Project@edit')->name('superadmin.info.edit');
Route::patch('superadmin.info.save.{id}', 'SuperAdmin\Project@saveedit')->name('superadmin.info.save');
Route::delete('superadmin.info.delete.{id}', 'SuperAdmin\Project@delete');

// //super admin hapus 
// Route::get('hapus_data/{admin}/destroy', 'SuperAdmin\AdminDivisi@destroy'); // hapus admin produksi
// Route::get('hapus_adm_account/{admin}/destroy', 'SuperAdmin\AdminDivisi@destroy'); // hapus admin account
// Route::get('hapus_adm_info/{admin}/destroy', 'SuperAdmin\AdminDivisi@destroy'); // hapus admin info

// Route::resource('info', 'SuperAdmin\InfoController');


//Admin account dashboard
Route::get('AdminAccount', 'AdminAccount\AdminAccount@index')->name('AdminAccount');
Route::get('AdminAccount.tambahprojek', 'AdminAccount\Project@index')->name('AdminAccount.tambahprojek');

Route::get('AdminAccount.project', 'AdminAccount\Project@index')->name('AdminAccount.project');
Route::get('AdminAccount.project.praproduksi', 'AdminAccount\Project@praproduksi')->name('AdminAccount.project.praproduksi');
Route::get('AdminAccount.project.produksi', 'AdminAccount\Project@produksi')->name('AdminAccount.project.produksi');
Route::get('AdminAccount.project.pascaproduksi', 'AdminAccount\Project@pascaproduksi')->name('AdminAccount.project.pascaproduksi');
Route::get('AdminAccount.tambah_project', 'AdminAccount\Project@tambah_project')->name('AdminAccount.tambah_project');
Route::post('AdminAccount.simpan_project', 'AdminAccount\Project@simpan_project')->name('AdminAccount.simpan_project');
Route::get('AdminAccount.project.client.{id_project}', 'AdminAccount\Project@client')->name('AdminAccount.project.client');
Route::patch('AdminAccount.project.simpan.{id_project}', 'AdminAccount\Project@update')->name('AdminAccount.project.simpan');
Route::get('AdminAccount.project.detailproject.{id_project}', 'AdminAccount\Project@detail')->name('AdminAccount.project.detailproject');
Route::get('AdminAccount.project.{id_project}.gantistatus', 'AdminAccount\Project@gantistatus')->name('AdminAccount.project.gantistatus');
Route::patch('AdminAccount.project.simpanstatus.{id_project}', 'AdminAccount\Project@simpanstatus')->name('AdminAccount.project.simpanstatus');

Route::get('AdminAccount.report.{id_project}', 'AdminAccount\Project@report')->name('AdminAccount.report');
Route::delete('AdminAccount.report.delete.{id_report}', 'AdminAccount\Project@delete')->name('AdminAccount.report.delete');
Route::get('AdminAccount.tambah_report', 'AdminAccount\Project@tambahreport')->name('AdminAccount.tambah_report');
Route::post('AdminAccount.image.upload', 'AdminAccount\Project@simpanreport')->name('AdminAccount.report.simpan');
Route::get('AdminAccount/report/detailreport/{id_report}', 'AdminAccount\Project@detailreport')->name('AdminAccount.report.detailreport');
Route::get('/AdminAccount/download/{file}', 'AdminAccount\Project@downloadreport');

Route::get('AdminAccount.admindivisi', 'AdminAccount\AdminDivisi@index')->name('AdminAccount.admindivisi');
Route::get('AdminAccount.admindivisi.adminaccount', 'AdminAccount\AdminDivisi@adminaccount')->name('AdminAccount.admindivisi.adminaccount');
Route::get('AdminAccount.admindivisi.admininfo', 'AdminAccount\AdminDivisi@admininfo')->name('AdminAccount.admindivisi.admininfo');
Route::get('AdminAccount.admindivisi.adminproduksi', 'AdminAccount\AdminDivisi@adminproduksi')->name('AdminAccount.admindivisi.adminproduksi');
Route::get('AdminAccount.admindivisi.{id}.gantistatus', 'AdminAccount\AdminDivisi@gantistatus')->name('AdminAccount.admindivisi.gantistatus');
Route::patch('AdminAccount.admindivisi.simpanstatus.{id}', 'AdminAccount\AdminDivisi@simpanstatus')->name('AdminAccount.admindivisi.simpanstatus');
Route::get('AdminAccount.admin.detail.{id}', 'AdminAccount\AdminDivisi@detail')->name('AdminAccount.admin.detail');

Route::get('AdminAccount.user', 'AdminAccount\AdminAccount@user')->name('AdminAccount.user');
Route::get('AdminAccount.user.tambah', 'AdminAccount\AdminAccount@tambahuser')->name('AdminAccount.user.tambah');
Route::post('AdminAccount.user.simpan', 'AdminAccount\AdminAccount@simpanuser')->name('AdminAccount.user.simpan');
Route::get('AdminAccount.user.client', 'AdminAccount\AdminAccount@client')->name('AdminAccount.user.client');
Route::get('AdminAccount.user.subclient', 'AdminAccount\AdminAccount@subclient')->name('AdminAccount.user.subclient');
Route::get('AdminAccount.subclient.tambah', 'AdminAccount\AdminAccount@tambahSubclient')->name('AdminAccount.subclient.tambah');
Route::post('AdminAccount.subclient.simpan', 'AdminAccount\AdminAccount@simpansubclient')->name('AdminAccount.subclient.simpan');
Route::get('AdminAccount.users.{id}.gantistatus', 'AdminAccount\AdminAccount@gantistatus')->name('AdminAccount.users.gantistatus');
Route::patch('AdminAccount.users.simpanstatus.{id_client}', 'AdminAccount\AdminAccount@simpanstatus')->name('AdminAccount.users.simpanstatus');
Route::get('AdminAccount.user.detail.{id}', 'AdminAccount\AdminAccount@detail')->name('AdminAccount.user.detail');

Route::get('AdminAccount.tiket', 'AdminAccount\Ticket@index')->name('AdminAccount.tiket');
Route::get('AdminAccount.tiket.detailtiket.{id_ticket}', 'AdminAccount\Ticket@detail')->name('AdminAccount.tiket.detailtiket');
Route::get('AdminAccount.tiket.{id_ticket}.gantistatus', 'AdminAccount\Ticket@gantistatus')->name('AdminAccount.tiket.gantistatus');
Route::patch('AdminAccount.tiket.simpanstatus.{id_ticket}', 'AdminAccount\Ticket@simpanstatus')->name('AdminAccount.tiket.simpanstatus');

Route::get('AdminAccount.Ticket_Client', 'AdminAccount\AdminTicket@Ticket')->name('AdminAccount.Ticket');
Route::get('AdminAccount.detail.{id_ticket}', 'AdminAccount\AdminTicket@detail')->name('AdminAccount.ticket.detail');
Route::post('AdminAccount.detail.{id_ticket}', 'KomentarController@komentarPost')->name('komentar.ticket');


Route::get('AdminAccount.informasi', 'AdminAccount\Project@info')->name('AdminAccount.informasi');
Route::get('AdminAccount.tambah_info', 'AdminAccount\Project@tambah_info')->name('AdminAccount.tambah_info');
Route::post('AdminAccount.simpan_info', 'AdminAccount\Project@simpan_info')->name('AdminAccount.simpan_info');


//profil edit user account
Route::get('Profil/{id}', 'UserProfilController@update')->name('edit.data');
Route::get('Profil2/{id}', 'UserProfilController@updateProfil')->name('edit.data2');
Route::patch('Profil/{id}', 'UserProfilController@updateProcess')->name('profil.edit');

Route::get('clientProfil/{id}', 'Client\AkunClient@update')->name('editdata1');
Route::get('clientProfil2/{id}', 'Client\AkunClient@updateProfil')->name('editdata3');
Route::patch('clientProfil/{id}', 'Client\AkunClient@updateProcess')->name('profil.edit1');

// Route::get('AdminAccount/Profil/{id}', 'UserProfilController@update')->name('edit.data');
// Route::get('AdminAccount/Profil2/{id}', 'UserProfilController@updateProfil')->name('edit.data2');
// Route::patch('admin/Profil/{id}', 'UserProfilController@updateProcess')->name('profil.edit');


//admin info dasboard
Route::get('AdminInfo', 'InfoAdmin\AdminInfo@index')->name('Admininfo');
Route::get('AdminInfo.project', 'InfoAdmin\Project@index')->name('AdminInfo.project');
Route::get('AdminInfo.project.praproduksi', 'InfoAdmin\Project@praproduksi')->name('AdminInfo.project.praproduksi');
Route::get('AdminInfo.project.produksi', 'InfoAdmin\Project@produksi')->name('AdminInfo.project.produksi');
Route::get('AdminInfo.project.pascaproduksi', 'InfoAdmin\Project@pascaproduksi')->name('AdminInfo.project.pascaproduksi');
Route::get('AdminInfo.project.{id_project}.gantistatus', 'InfoAdmin\Project@gantistatus')->name('AdminInfo.project.gantistatus');
Route::patch('AdminInfo.project.simpanstatus.{id_project}', 'InfoAdmin\Project@simpanstatus')->name('AdminInfo.project.simpanstatus');
Route::get('AdminInfo.project.detailproject.{id_project}', 'InfoAdmin\Project@detail')->name('AdminInfo.project.detailproject');

Route::get('AdminInfo.admindivisi', 'InfoAdmin\AdminDivisi@index')->name('AdminInfo.admindivisi');
Route::get('AdminInfo.adminaccount', 'InfoAdmin\AdminDivisi@adminaccount')->name('AdminInfo.adminaccount');
Route::get('AdminInfo.admininfo', 'InfoAdmin\AdminDivisi@admininfo')->name('AdminInfo.admininfo');
Route::get('AdminInfo.adminproduksi', 'InfoAdmin\AdminDivisi@adminproduksi')->name('AdminInfo.adminproduksi');
Route::get('AdminInfo.admin.detail.{id}', 'InfoAdmin\AdminDivisi@detail')->name('AdminInfo.admin.detail');

Route::get('AdminInfo.ticket', 'InfoAdmin\Ticket@index')->name('AdminInfo.ticket');
Route::get('AdminInfo.tiket.detailtiket.{id_ticket}', 'InfoAdmin\Ticket@detail')->name('AdminInfo.tiket.detailtiket');
Route::get('AdminInfo.tiket.{id_ticket}.gantistatus', 'InfoAdmin\Ticket@gantistatus')->name('AdminInfo.tiket.gantistatus');
Route::patch('AdminInfo.tiket.simpanstatus.{id_ticket}', 'InfoAdmin\Ticket@simpanstatus')->name('AdminInfo.tiket.simpanstatus');
Route::get('AdminInfo.Ticket_Client', 'InfoAdmin\Ticket@Ticket')->name('AdminInfo.Ticket');

Route::get('AdminInfo.report.{id_project}', 'InfoAdmin\Project@report')->name('AdminInfo.report');
Route::delete('AdminInfo.report.delete.{id_report}', 'InfoAdmin\Project@delete')->name('AdminInfo.report.delete');
Route::get('AdminInfo.tambah_report', 'InfoAdmin\Project@tambahreport')->name('AdminInfo.tambah_report');
Route::post('AdminInfo.image.upload', 'InfoAdmin\Project@simpanreport')->name('AdminInfo.report.simpan');
Route::get('AdminInfo/report/detailreport/{id_report}', 'InfoAdmin\Project@detailreport')->name('AdminInfo.report.detailreport');
Route::get('/AdminInfo/download/{file}', 'InfoAdmin\Project@downloadreport');

Route::get('AdminInfo.users', 'InfoAdmin\User@index')->name('AdminInfo.users');
Route::get('AdminInfo.users.client', 'InfoAdmin\User@client')->name('AdminInfo.users.client');
Route::get('AdminInfo.users.subclient', 'InfoAdmin\User@subclient')->name('AdminInfo.users.subclient');
Route::get('AdminInfo.user.detail.{id}', 'InfoAdmin\User@detail')->name('AdminInfo.user.detail');

Route::get('AdminInfo.informasi', 'InfoAdmin\Project@info')->name('AdminInfo.informasi');
Route::get('AdminInfo.tambah_info', 'InfoAdmin\Project@tambah_info')->name('AdminInfo.tambah_info');
Route::post('AdminInfo.simpan_info', 'InfoAdmin\Project@simpan_info')->name('AdminInfo.simpan_info');


//admin produksi dashboard
Route::get('AdminProduksi', 'ProduksiAdmin\AdminProduksi@index')->name('AdminProduksi');
Route::get('AdminProduksi.ticket', 'ProduksiAdmin\Ticket@index')->name('AdminProduksi.dataticket');
Route::get('AdminProduksi.project', 'ProduksiAdmin\Project@index')->name('AdminProduksi.project');
Route::get('AdminProduksi.user', 'ProduksiAdmin\AdminProduksi@user')->name('AdminProduksi.user');
Route::get('AdminProduksi.admindivisi', 'ProduksiAdmin\AdminProduksi@admin')->name('AdminProduksi.admindivisi');

Route::get('AdminProduksi.tiket.detailtiket.{id_ticket}', 'ProduksiAdmin\Ticket@detail')->name('AdminProduksi.tiket.detailtiket');
Route::get('AdminProduksi.tiket.{id_ticket}.gantistatus', 'ProduksiAdmin\Ticket@gantistatus')->name('AdminProduksi.tiket.gantistatus');
Route::patch('AdminProduksi.tiket.simpanstatus.{id_ticket}', 'ProduksiAdmin\Ticket@simpanstatus')->name('AdminProduksi.tiket.simpanstatus');
Route::get('AdminProduksi.Ticket_Client', 'ProduksiAdmin\Ticket@Ticket')->name('AdminProduksi.Ticket');

Route::get('AdminProduksi.user.client', 'ProduksiAdmin\AdminProduksi@client')->name('AdminProduksi.user.client');
Route::get('AdminProduksi.user.subclient', 'ProduksiAdmin\AdminProduksi@subclient')->name('AdminProduksi.user.subclient');
Route::get('AdminProduksi.user.detail.{id}', 'ProduksiAdmin\AdminProduksi@detailsub')->name('AdminProduksi.user.detail');

Route::get('AdminProduksi.admindivisi.adminaccount', 'ProduksiAdmin\AdminProduksi@adminaccount')->name('AdminProduksi.admindivisi.adminaccount');
Route::get('AdminProduksi.admindivisi.admininfo', 'ProduksiAdmin\AdminProduksi@admininfo')->name('AdminProduksi.admindivisi.admininfo');
Route::get('AdminProduksi.admindivisi.adminproduksi', 'ProduksiAdmin\AdminProduksi@adminproduksi')->name('AdminProduksi.admindivisi.adminproduksi');
Route::get('AdminProduksi.admin.detail.{id}', 'ProduksiAdmin\AdminProduksi@detail')->name('AdminProduksi.admin.detail');

Route::get('AdminProduksi.project.praproduksi', 'ProduksiAdmin\Project@praproduksi')->name('AdminProduksi.project.praproduksi');
Route::get('AdminProduksi.project.produksi', 'ProduksiAdmin\Project@produksi')->name('AdminProduksi.project.produksi');
Route::get('AdminProduksi.project.pascaproduksi', 'ProduksiAdmin\Project@pascaproduksi')->name('AdminProduksi.project.pascaproduksi');
Route::get('AdminProduksi.project.{id_project}.gantistatus', 'ProduksiAdmin\Project@gantistatus')->name('AdminProduksi.project.gantistatus');
Route::patch('AdminProduksi.project.simpanstatus.{id_project}', 'ProduksiAdmin\Project@simpanstatus')->name('AdminProduksi.project.simpanstatus');
Route::get('AdminProduksi.project.detailproject.{id_project}', 'ProduksiAdmin\Project@detail')->name('AdminProduksi.project.detailproject');

Route::get('AdminProduksi.report.{id_project}', 'ProduksiAdmin\Project@report')->name('AdminProduksi.report');
Route::delete('AdminProduksi.report.delete.{id_report}', 'ProduksiAdmin\Project@delete')->name('AdminProduksi.report.delete');
Route::get('AdminProduksi/report/detailreport/{id_report}', 'ProduksiAdmin\Project@detailreport')->name('AdminProduksi.report.detailreport');
Route::get('/AdminProduksi/download/{file}', 'ProduksiAdmin\Project@downloadreport');
Route::get('AdminProduksi.tambah_report', 'ProduksiAdmin\Project@tambahreport')->name('ProduksiAdmin.tambah_report');
Route::post('AdminProduksi.image.upload', 'ProduksiAdmin\Project@simpanreport')->name('ProduksiAdmin.report.simpan');

Route::get('AdminProduksi.informasi', 'ProduksiAdmin\Project@info')->name('AdminProduksi.informasi');
Route::get('AdminProduksi.tambah_info', 'ProduksiAdmin\Project@tambah_info')->name('AdminProduksi.tambah_info');
Route::post('AdminProduksi.simpan_info', 'ProduksiAdmin\Project@simpan_info')->name('AdminProduksi.simpan_info');


//client
Route::get('client', 'Client\UserClient@index')->name('userclient');
Route::get('client', 'Client\UserClient@index')->name('userclient');
Route::get('client.projectsaya', 'Client\Project@Project')->name('client.projectsaya');
Route::get('client.sublient', 'Client\SubClient@subClient')->name('SubClient');

Route::get('client.project.client.{id_project}', 'Client\Project@client')->name('client.project.client');
Route::patch('client.project.simpan.{id_project}', 'Client\Project@update')->name('client.project.simpan');
Route::get('client.project.detailproject.{id_project}', 'Client\Project@detail')->name('client.project.detailproject');

Route::get('client.user.tambah', 'Client\UserClient@tambah')->name('client.user.tambah');
Route::post('client.user.simpan', 'Client\UserClient@simpan')->name('client.user.simpan');

Route::get('Client.subclient.{id}.gantistatus', 'Client\UserClient@gantistatus')->name('Client.subclient.gantistatus');
Route::patch('Client.subclient.simpanstatus.{id}', 'Client\UserClient@simpanstatus')->name('Client.subclient.simpanstatus');

Route::get('client.report.{id_project}', 'Client\Project@report')->name('client.report');
Route::get('client/report/detailreport/{id_report}', 'Client\Project@detailreport')->name('client.report.detailreport');
Route::get('/client/download/{file}', 'Client\Project@downloadreport');

// Route::get('client.ticket', 'Client\Ticket@Ticket')->name('client.ticket');
// Route::get('client.ticket.tambah', 'Client\Ticket@tambah')->name('client.ticket.tambah');
// Route::post('client.ticket.simpan', 'Client\Ticket@simpan')->name('client.ticket.simpan');
// Route::get('client.ticket.detail.{id_ticket}', 'Client\Ticket@detail')->name('client.ticket.detail');
Route::get('client.ticket', 'Client\Ticket@Ticket')->name('client.ticket');
Route::get('client.ticket.tambah', 'Client\Ticket@tambah')->name('client.ticket.tambah');
Route::post('client.ticket.simpan', 'Client\Ticket@simpan')->name('client.ticket.simpan');
Route::get('client.detail.{id_ticket}', 'Client\Ticket@detail')->name('client.ticket.detail');
Route::post('client.detail.{id_ticket}', 'KomentarController@komentarPost')->name('komentar.ticket');


Route::get('client.informasi', 'Client\Project@info')->name('client.informasi');


//SubClient
Route::get('usersubclient', 'SubClient\UserSubclient@index')->name('usersubclient');
Route::get('subclient', 'SubClient\UserSubclient@index')->name('subclient');
Route::get('subclient.projectsaya', 'SubClient\Project@Project')->name('subclient.projectsaya');
Route::get('subclient.detailproject.{id_project}', 'SubClient\Project@detail')->name('subclient.detailproject');
Route::get('subclient.report.{id_project}', 'SubClient\Project@report')->name('subclient.report');
Route::get('subclient/report/detailreport/{id_report}', 'SubClient\Project@detailreport')->name('subclient.report.detailreport');
Route::get('/subclient/download/{file}', 'SubClient\Project@downloadreport');

// Route::get('subclient.ticket', 'SubClient\Ticket@Ticket')->name('subclient.ticket');
// Route::get('subclient.ticket.tambah', 'SubClient\Ticket@tambah')->name('subclient.ticket.tambah');
// Route::post('subclient.ticket.simpan', 'SubClient\Ticket@simpan')->name('subclient.ticket.simpan');
// Route::get('subclient.ticket.detail.{id_ticket}', 'SubClient\Ticket@detail')->name('subclient.ticket.detail');
Route::get('subclient.ticket', 'SubClient\Ticket@Ticket')->name('subclient.ticket');
Route::get('subclient.ticket.tambah', 'SubClient\Ticket@tambah')->name('subclient.ticket.tambah');
Route::post('subclient.ticket.simpan', 'SubClient\Ticket@simpan')->name('subclient.ticket.simpan');
Route::get('subclient.detail.{id_ticket}', 'SubClient\Ticket@detail')->name('subclient.ticket.detail');
Route::post('subclient.detail.{id_ticket}', 'KomentarController@komentarPost')->name('komentar.ticket');

Route::get('subclient.informasi', 'SubClient\Project@info')->name('subclient.informasi');


//auth login
Route::post('/admin/login', 'AdminAuthController@postLogin');
Route::get('/user/login', 'UserAuthController@formLogin');
// Route::get('/admin/login', 'AdminAuthController@formLogin');
Route::post('/user/postlogin', 'UserAuthController@postLogin');
Route::get('register', 'AdminAuthController@showRegister');
Route::post('register', 'AdminAuthController@register');

//send email
Route::post('sendEmail', 'SuperAdmin\AdminDivisi@SendEmail')->name('kirim.email');
Route::post('kirimEmail', 'SuperAdmin\User@SendEmail2')->name('kirim.email5');
Route::post('kirim_email', 'UserForgotPwController@Email')->name('kirim.email2');
Route::post('email_reset', 'UserForgotPwController@Emailreset')->name('email.resetpw');
Route::post('Email_Subclient', 'Client\UserClient@Email2')->name('kirim.email3');
Route::post('Email_Client', 'AdminAccount\AdminAccount@KirimEmail')->name('kirim.email4');

// Auth::routes(['verify' => true]);
Route::get('/user', 'UserAuthController@home');
Route::get('verify/{email}/{secret}/{?uer_type}', 'Auth\verificationController@verifyEmail');


Route::get('/', 'AdminAuthController@formLogin')->name('login');
Route::get('register', 'AuthController@showFormRegister')->name('register');
Route::post('register', 'AuthController@register');

Route::group(['middleware' => 'auth'], function () {

    Route::get('home', 'HomeController@index')->name('home');
    Route::get('logout', 'AuthController@logout')->name('logout');
});

//edit user password

Route::get('superadmin/ubahPassword/{id}', 'UserProfilController@updatePw')->name('user.update.pw');
Route::patch('super_admin/ubahPassword/{id}', 'UserProfilController@updatePassword')->name('user.updatePassword');

Route::post('super_admin/ubahPassword/{id}', 'UserProfilController@updatePassword')->name('user.updatePassword');

//forgot password
Route::get('user/forgotpw', 'UserForgotPwController@index')->name('forgotpw');
Route::post('user/forgotpw2', 'UserForgotPwController@tampil')->name('forgotpw2');
Route::get('user/forgotpw2', 'UserForgotPwController@tampil')->name('forgotpw2');
Route::patch('user/forgotpw3', 'UserForgotPwController@forgotPw')->name('forgotpw3');
