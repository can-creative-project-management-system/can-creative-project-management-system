-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 20, 2020 at 04:46 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `can_creative_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `adm_role` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tlp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Aktif','Nonaktif') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `adm_role`, `name`, `email`, `tlp`, `alamat`, `file`, `password`, `status`, `remember_token`, `email_verified_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Asna', 'iinasnawiyah4@gmail.com', '089765345675', 'Indramayu', '', '$2y$10$0iLjzyfp61/LEkglGOsNhOWjCXmwWcachdh2NhxHuUNqX.rMYYhoi', 'Nonaktif', NULL, NULL, NULL, NULL),
(3, 2, 'iin ', 'iinasnawiyah@gmail.com', '0895411597593', 'singaraja', '', '$2y$10$skjx6f.cUAtvabCQQyfZxunNfNK7l7oy.ndsHgHIQxM9/2Ob5wBSe', 'Aktif', 'admin', '0000-00-00 00:00:00', NULL, NULL),
(4, 3, 'shafa', 'failusufashafa11@gmail.com', '087654324567', 'Indramayu', '', '$2y$10$0iLjzyfp61/LEkglGOsNhOWjCXmwWcachdh2NhxHuUNqX.rMYYhoi', 'Aktif', NULL, NULL, NULL, NULL),
(14, 4, 'ari', 'arifin@gmail.com', '0897865664446', 'kaplongan', NULL, '$2y$10$0iLjzyfp61/LEkglGOsNhOWjCXmwWcachdh2NhxHuUNqX.rMYYhoi', 'Aktif', NULL, NULL, NULL, NULL),
(15, 4, 'Wantrisnadi', 'failusufashaasddasfa11@gmail.com', NULL, 'Jl. Murahnara, Sindang, Kabupaten Indramayu, Jawa Barat 45222as', NULL, 'yuni', 'Aktif', NULL, NULL, '2020-12-07 20:23:13', '2020-12-07 20:23:13'),
(17, 2, 'sada', 'failusufashaasdafa11@gmail.com', NULL, 'singajaya', NULL, 'yuni', 'Aktif', NULL, NULL, '2020-12-07 20:29:34', '2020-12-07 20:29:34'),
(19, 2, 'sada', 'dafa11@gmail.com', NULL, 'singajaya', NULL, '$2y$10$WlGbfx89oVGCIvQQnMnYcO9lH0WlEl0XtjDdi1P.XMHjAr4MiGqXS', 'Nonaktif', NULL, NULL, '2020-12-07 20:36:59', '2020-12-07 20:36:59'),
(40, 1, 'Iin Asnawiyah', 'iinasnawiy2222ah4@gmail.com', NULL, 'Blok Jl. Wanasari, Karangsong, Kec. Indramayu, Kabupaten Indramayu, Jawa Barat', NULL, '$2y$10$5MqHZX9H3I256bIAVrOUwuFba9ByXqdUbEF4YuZ9N2GKuWBga7xLG', 'Nonaktif', NULL, NULL, '2020-12-10 03:15:13', '2020-12-10 03:15:13'),
(41, 3, 'Iin Asnawiyah', 'iinasnaw1111iyah@gmail.com', NULL, 'Blok Jl. Wanasari, Karangsong, Kec. Indramayu, Kabupaten Indramayu, Jawa Barat', NULL, '$2y$10$46zCWBewBUQwSG3Su2vh8uBLlwexNqKVXPNI3PQBfFy//pTO1FozS', 'Nonaktif', NULL, NULL, '2020-12-10 03:16:14', '2020-12-10 03:16:14'),
(42, 4, 'rohma', 'rohmatuljannah@gmail.com', NULL, 'Jl. Murahnara, Sindang, Kabupaten Indramayu, Jawa Barat 45222', NULL, '$2y$10$jgOb.rhLZISC/4G.f.I/gepilOScCd7fbUoDfGoDHo8fw.S9UW4D.', 'Nonaktif', NULL, NULL, '2020-12-10 03:16:39', '2020-12-10 03:16:39'),
(43, 3, 'Iin Asnawiyah', 'iinasnawwwwiyah@gmail.com', NULL, 'Jl. Murahnara, Sindang, Kabupaten Indramayu, Jawa Barat 45222', NULL, '$2y$10$br/M7w5n1iZz3V8MTfNMyut5ey.GJ8E1KWzZ5im.GauIqDkvYN/2G', 'Nonaktif', NULL, NULL, '2020-12-10 03:18:53', '2020-12-10 03:18:53'),
(54, 4, 'sebura', 'seburapolindra20@gmail.com', '089765446655', 'polindra', NULL, '$2y$10$VUD7ep69rkXqlSYjimx2Sum1sLyu8R5G4TxfJ2LmIVlGjYkxC31ge', 'Nonaktif', NULL, NULL, NULL, NULL),
(57, 1, 'ariin', 'ariin2915@gmail.com', NULL, 'Lemahabang, Kec. Indramayu, Kabupaten Indramayu, Jawa Barat 45212', NULL, '$2y$10$lIUIFKdXsMqURkVJXnzQtenDcqjbr5BjOfqG6Row2pBZs0C6my45C', 'Nonaktif', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tlp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` enum('Superadmin','AdminAccount','AdminInfo','AdminProduksi') COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Aktif','Nonaktif') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(34, '2014_10_12_000000_create_users_table', 1),
(35, '2014_10_12_100000_create_password_resets_table', 1),
(36, '2019_08_19_000000_create_failed_jobs_table', 1),
(37, '2020_12_02_035108_create_role_user_table', 1),
(38, '2020_12_02_042200_create_admin', 1),
(39, '2020_12_02_072641_create_role_admin_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id_project` int(10) UNSIGNED NOT NULL,
  `id_client` bigint(20) UNSIGNED DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `status` enum('Pra','Produksi','Pasca') NOT NULL,
  `mou` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `due` date NOT NULL,
  `maintenance` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id_project`, `id_client`, `nama`, `detail`, `status`, `mou`, `logo`, `due`, `maintenance`, `created_at`, `updated_at`) VALUES
(2, 4, 'Y.O.U Ethereal Beauty', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'Pra', '1608217763_uas.xlsx', '1608217763_icon_pantai.jpg', '2021-02-01', NULL, '2020-12-17 08:09:23', '2020-12-17 08:09:23'),
(3, 4, 'Si Pariwisata Indramayu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'Produksi', '1608217826_2.jpg', '1608217826_Agrowisata-Situ-Bolang-Indramayu.jpg', '2021-04-17', NULL, '2020-12-17 08:10:26', '2020-12-17 08:10:26'),
(4, NULL, 'Beyond The Dream', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'Pasca', '1608217890_4.jpg', '1608217890_imageresizer.php.jpg', '2021-05-16', NULL, '2020-12-17 08:11:30', '2020-12-17 08:11:30'),
(5, 5, 'Makam Raden Arya Wiralodra', 'makam raden arya wiralodra', 'Pra', '1608255379_3.jpg', '1608255379_3.jpg', '2020-12-08', NULL, '2020-12-17 18:36:19', '2020-12-17 18:36:19'),
(6, NULL, 'Lenovo Service Center', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'Pasca', '1608284345_project_management_system(3).sql', '1608284345_1280px-BNI_logo.svg.png', '2021-06-18', NULL, '2020-12-18 02:39:05', '2020-12-18 02:39:05');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id_report` int(10) UNSIGNED NOT NULL,
  `id_project` int(10) UNSIGNED DEFAULT NULL,
  `subject` text NOT NULL,
  `changelog` text NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `link` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`id_report`, `id_project`, `subject`, `changelog`, `file`, `link`, `created_at`, `updated_at`) VALUES
(1, 2, 'subject', 'changelog', '1608275563_project_management_system(3).sql', 'http://127.0.0.1:8000/superadmin.report.tambah', '2020-12-18 00:12:43', '2020-12-18 00:12:43'),
(2, 3, 'Project sedang dalam tahap pembuatan mockup', 'mock up', '1608281466_project_management_system(3).sql', 'http://127.0.0.1:8000/superadmin.report.tambah', '2020-12-18 01:51:07', '2020-12-18 01:51:07'),
(3, 2, 'Project sedang dalam tahap pembuatan mockup', 's', '1608281543_project_management_system(3).sql', 'http://127.0.0.1:8000/superadmin.report.tambah', '2020-12-18 01:52:24', '2020-12-18 01:52:24'),
(4, 5, 'Project sedang dalam tahap pembuatan mockup', 'makam raden arya', '1608281692_project_management_system(3).sql', 'http://127.0.0.1:8000/superadmin.report.tambah', '2020-12-18 01:54:52', '2020-12-18 01:54:52');

-- --------------------------------------------------------

--
-- Table structure for table `role_admin`
--

CREATE TABLE `role_admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `adm_role_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_admin`
--

INSERT INTO `role_admin` (`id`, `adm_role_name`) VALUES
(1, 'Admin Utama'),
(2, 'Super_Admin'),
(3, 'Admin Produksi'),
(4, 'Admin Info');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_role_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_role_name`) VALUES
(1, 'user client'),
(2, 'user subclient');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_client` bigint(20) UNSIGNED DEFAULT NULL,
  `user_role` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Aktif','Nonaktif') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_client`, `user_role`, `name`, `email`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'asna', 'asnarin@gmail.com', NULL, '$2y$10$0iLjzyfp61/LEkglGOsNhOWjCXmwWcachdh2NhxHuUNqX.rMYYhoi', 'Aktif', NULL, NULL, NULL),
(3, NULL, 2, 'nurul', 'nurulkhotimah@gmail.com', NULL, '$2y$10$0iLjzyfp61/LEkglGOsNhOWjCXmwWcachdh2NhxHuUNqX.rMYYhoi', 'Aktif', NULL, NULL, NULL),
(4, NULL, 1, 'maias', 'maias29159800@gmmail.com', NULL, 'maias', 'Aktif', NULL, NULL, NULL),
(5, NULL, 1, 'mohamad amin', 'mohamadamin@gmail.com', NULL, '$2y$10$vkHwLI/.Nx9yC.MBkicXieEPAIT4eWVaMJVeDSmO/HnAfS.sbMGuq', 'Aktif', NULL, '2020-12-17 09:14:35', '2020-12-17 09:14:35'),
(6, NULL, 1, 'ghhhhgg', 'iinasnawiyah@gmail.com', NULL, '$2y$10$hzF.Mvf3FEV7QQOF0sC9oOWuJQa94we1hNyis092/9YFZazp73jN6', 'Aktif', NULL, '2020-12-17 18:32:18', '2020-12-17 18:32:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_email_unique` (`email`),
  ADD KEY `admin_adm_role_foreign` (`adm_role`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id_project`),
  ADD KEY `id_client` (`id_client`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id_report`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `role_admin`
--
ALTER TABLE `role_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_user_role_foreign` (`user_role`),
  ADD KEY `id_client` (`id_client`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id_project` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id_report` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_admin`
--
ALTER TABLE `role_admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_adm_role_foreign` FOREIGN KEY (`adm_role`) REFERENCES `role_admin` (`id`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `users` (`id`);

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `id_project` FOREIGN KEY (`id_project`) REFERENCES `project` (`id_project`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `id_client` FOREIGN KEY (`id_client`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_user_role_foreign` FOREIGN KEY (`user_role`) REFERENCES `role_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
