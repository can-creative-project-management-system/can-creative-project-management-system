<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\report_model;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Foreach_;

class Report extends Controller
{


    public function index($id_project)
    {
        $proyek = DB::table('report')->where('id_project', $id_project)
            ->orderBy('id_report', 'DESC')
            ->get();
        return view('beckend.super_admin.report', ['proyek' => $proyek]);
    }
    public function tambah()
    {
        return view('beckend.super_admin.report_tambah');
    }
    public function simpan(Request $request)
    {

        try {
            $datareport = report_model::create([
                'id_project' => $request->id_project,
                'subject' => $request->subject,
                'changelog' => $request->changelog,
                'link' => $request->link,
            ]);
            $datareport = report_model::where("subject", $request->subject)->first();

            if ($request->hasFile('file')) {
                $files = $request->file('file');

                foreach ($files as $file) {
                    $nama_file = time() . "_" . $file->getClientOriginalName();

                    // $destinationPath = 'report';
                    // $file->move($destinationPath, $nama_file);
                    $nama_file[] = $file->store('report');
                }
                // $datareport->update([
                //     'file' => json_encode($nama_file),
                // ]);
                $admin = DB::table('file')->insert([
                    'id_report' => $datareport->id_report,
                    'file' => $nama_file,
                ]);

                return view('beckend.super_admin.report_tambah', ['admin' => $admin])->with('pop up', 'Project Berhasil Dibuat!');
            }
            return 'Empty Files';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function detail($id_report)
    {
        // dd($id_report);
        $project = report_model::where('id_report', $id_report)
            ->join('project', 'report.id_project', '=', 'project.id_project')
            ->select('report.*', 'project.nama')
            ->first();

        return view('beckend.super_admin.Report-detail', ['DataAdmin' => $project]);
    }

    public function edit($id_report)
    {
        $report = DB::table('report')->where('id_report', $id_report)->first();
        return view('beckend.super_admin.Report-edit', ['report' => $report]);
    }

    public function simpanedit(Request $request, $id_report)
    {
        // dd($request->all());
        $proyek = DB::table('report')->where('id_report', $id_report)->update(
            [
                'subject' => $request->subject,
                'updated_at' => $request->updated_at,
                'changelog' => $request->changelog,
                'link' => $request->link,
                'file' => $request->file,
            ]
        );

        return view('beckend.super_admin.report', ['proyek' => $proyek]);
    }
    public function delete($id_report)
    {
        DB::table('report')->where('id_report', $id_report)->delete();
        return back()->with('status', 'Data Report Berhasil Dihapus');
    }

    public function download($file)
    {
        return response()->download('report/' . $file);
    }
}
