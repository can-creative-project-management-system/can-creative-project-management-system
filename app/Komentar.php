<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['id', 'id_ticket', 'id_admin', 'id_user', 'id_project', 'komentar', 'parent', 'created_at', 'updated_at'];

    protected $table = 'komentars';

    protected $timestampts = true;

    public function user()
    {
        return $this->belongsTo(users::class);
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    // public function childs()
    // {
    //     return $this->hasMany(Komentar::class, 'parent');
    // }

    public function admin()
    {
        return $this->belongsTo(admin::class);
    }
}
