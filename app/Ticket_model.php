<?php

namespace App;

use App\Model\Admin;
use Illuminate\Database\Eloquent\Model;

class Ticket_model extends Model
{
    protected $table = "tickets";
    public $timestamps = true;
    protected $guarded = ['updated_at'];
    protected $primaryKey = 'id_ticket';

    public function komentar()
    {
        return $this->hasMany(Komentar::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
