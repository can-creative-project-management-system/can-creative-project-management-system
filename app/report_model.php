<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class report_model extends Model
{
    protected $table = "report";
    protected $primaryKey = "id_report";
    protected $fillable = ["id_project", "subject", "changelog", "file", "link"];
    protected $hidden = ['created_at', 'updated_at'];
    
}
