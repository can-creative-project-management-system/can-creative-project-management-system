<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;


class UserProfilController extends Controller
{


    //tampil update profil
    public function update($id)
    {
        $admin = DB::table('admin')->where('id', $id)->first();
        return view('frontend/admin/Profil', ['admin' => $admin]);
       

    }

    //tampil update profil updateProfil
    public function updateProfil($id)
    {
        $admin = DB::table('admin')->where('id', $id)->first();
        return view('frontend/admin/edit', ['admin' => $admin]);
       

    }

    //simpan updat eprofil
    public function updateProcess(Request $request, $id)
    {

        // dd($request->all());
        DB::table('admin')->where('id', Session::get('id'))->update(
            [
                'name' => $request->name,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'tlp' => $request->tlp,

            ]
        );
        return redirect()->route('edit.data', $id)->with('statusSuccess', 'Data Berhasil Di Update');
    }


    //tampil update Password
    public function updatePw($id)
    {
        $admin = DB::table('admin')->where('id', $id)->first();
        return view('password.ubahPassword', ['admin' => $admin]);
        // return view('user.profil.akun');

    }

    //Update password
    public function updatePassword(Request $request, $id)
    {

        $sandi_baru = $request->sandi_baru;
        if ($sandi_baru === $request->konfirmasi) {
            DB::table('admin')->where('id', $id)->update([
                'password' => Hash::make($request->sandi_baru),
            ]);
            return redirect()->back()->with("editpw", "Berhasil Ganti Password");
        } else {
            return redirect()->back()->with("failedpw", "Gagal Ganti Password");
        }
    }
}
