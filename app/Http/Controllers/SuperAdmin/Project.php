<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\project_model;
use App\information_model;
use Illuminate\Support\Facades\DB;

class Project extends Controller
{
    public function index()
    {
        return view('beckend.super_admin.Project');
    }
    public function praproduksi()
    {
        $proyek = project_model::where('status', '=', 'Pra')->get();
        return view('beckend.super_admin.Project-praproduksi', ['proyek' => $proyek]);
    }
    public function produksi()
    {
        $proyek = project_model::where('status', '=', 'Produksi')->get();
        return view('beckend.super_admin.Project-produksi', ['proyek' => $proyek]);
    }
    public function pascaproduksi()
    {
        $proyek = project_model::where('status', '=', 'Pasca')->get();
        return view('beckend.super_admin.Project-pascaproduksi', ['proyek' => $proyek]);
    }

    public function tambah_project()
    {

        return view('beckend.super_admin.TambahProject');
    }

    public function simpan_project(Request $request)
    {
        $file = $request->file('mou');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $destinationPath = 'mou';
        $file->move($destinationPath, $nama_file);

        $logo = $request->file('logo');

        $nama_logo = time() . "_" . $logo->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $destinationPath = 'logo';
        $logo->move($destinationPath, $nama_logo);

        $admin = project_model::create([

            'nama' => $request->nama,
            'due' => $request->due,
            'detail' => $request->detail,
            'status' => $request->status,
            'mou' => $nama_file,
            'logo' => $nama_logo


        ]);


        return redirect('superadmin.project')->with('status', 'Project Berhasil Dibuat!');
    }

    public function client($id_project)
    {
        $proyek = DB::table('project')->where('id_project', $id_project)->first();
        return view('beckend.super_admin.tambahclient', ['proyek' => $proyek]);
    }
    public function update(Request $request, $id_project)
    {

        // dd($request->all());
        $proyek = DB::table('project')->where('id_project', $id_project)->update(
            [
                'id_client' => $request->id_client,
            ]
        );
        return redirect()->route('superadmin.project')->with('statusSuccess', 'Data Berhasil Di Update');
    }

    public function gantistatus($id_project)
    {
        $proyek = DB::table('project')->where('id_project', $id_project)->first();
        return view('beckend.super_admin.Project-gantistatus', ['proyek' => $proyek]);
    }

    public function simpanstatus(Request $request, $id_project)
    {
        // dd($request->all());
        $proyek = DB::table('project')->where('id_project', $id_project)->update(
            [
                'status' => $request->status,
                'updated_at' => $request->updated_at,

            ]
        );

        return back()->with('status', 'Status berhasil dirubah');
    }
    public function detail($id_project)
    {
        $proyek = DB::table('project')->where('id_project', $id_project)->get();
        return view('beckend.super_admin.Project-detail', ['proyek' => $proyek]);
    }


    //info
    public function info()
    {
        $info = DB::table('information')
            ->orderBy('id', 'desc')
            ->get();

        return view('beckend.super_admin.info', ['info' => $info]);
    }

    public function tambah_info()
    {

        return view('beckend.super_admin.tambah_info');
    }

    public function simpan_info(Request $request)
    {

        $ticket = information_model::create([
            'module' => $request->module,
            'keterangan' => $request->keterangan,
        ]);


        return redirect()->route('superadmin.informasi')->with('popup', 'Informasi Baru Berhasil Ditambahkan!');
    }
    public function ubahstatus($id)
    {
        $info = DB::table('information')->where('id', $id)->first();
        return view('beckend.super_admin.info-gantistatus', ['info' => $info]);
    }

    public function savestatus(Request $request, $id)
    {
        $proyek = DB::table('information')->where('id', $id)->update(
            [
                'status' => $request->status,
            ]
        );
        return back()->with('status', 'Status berhasil dirubah');
    }
    public function edit($id)
    {
        $info = DB::table('information')->where('id', $id)->first();
        return view('beckend.super_admin.info_edit', ['info' => $info]);
    }

    public function saveedit(Request $request, $id)
    {
        $proyek = DB::table('information')->where('id', $id)->update(
            [
                'module' => $request->module,
                'keterangan' => $request->keterangan,
            ]
        );
        return back()->with('popup', 'Informasi User Guide berhasil dirubah');
    }
    public function delete($id)
    {
        // DB::table('information')->where('id', $id)->delete();
        information_model::destroy($id);
        return back()->with('popup', 'Data Informasi User Guide Berhasil Dihapus');
    }
}
