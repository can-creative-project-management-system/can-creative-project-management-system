<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Akses;


class AdminDivisi extends Controller
{
    public function index()
    {
        $admin = DB::table('admin')->get();
        return view('beckend.super_admin.AdminDivisi', ['admin' => $admin]);
    }


    //admin account
    public function adminaccount()
    {
        $admin = admin::where('adm_role', '=', '1')->get();
        return view('beckend.super_admin.AdminDivisi_adminaccount', ['admin' => $admin]);
    }

    public function tambah_adm_account()
    {

        return view('beckend.super_admin.Tambahadmin_account');
    }
    public function simpan_adm_account(Request $request)
    {
        // dd($request->all());
        $admin = admin::create([

            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make('$request->password'),
            'alamat' => $request->alamat,
            'adm_role' => $request->adm_role,

        ]);

        return redirect()->route('superadmin.admindivisi.adminaccount')->with('status', 'Data Admin Berhasil Ditambahkan!');
        // ('beckend.super_admin.AdminDivisi_adminproduksi')
    }

    public function destroy_adm_account(Admin $admin)
    {
        Admin::destroy($admin->id);
        return redirect('/superadmin.admindivisi.adminaccount')->with('status', 'Data Wisata Berhasil Dihapus!');
    }


    //admin info
    public function admininfo()
    {

        $admin = admin::where('adm_role', '=', '4')->get();
        return view('beckend.super_admin.AdminDivisi_admininfo', ['admin' => $admin]);
    }
    public function tambah_adm_info()
    {

        return view('beckend.super_admin.TambahAdmin_info');
    }
    public function simpan_adm_info(Request $request)
    {
        $admin = admin::create([

            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'alamat' => $request->alamat,
            'adm_role' => $request->adm_role,

        ]);
        return redirect()->route('superadmin.admindivisi.admininfo')->with('status', 'Data Admin Berhasil Ditambahkan!');
        // ('beckend.super_admin.AdminDivisi_adminproduksi')
    }

    public function destroy_adm_info(Admin $admin)
    {
        Admin::destroy($admin->id);
        return redirect()->route('superadmin.admindivisi.admininfo')->with('status', 'Data Wisata Berhasil Dihapus!');
    }




    //admin produksi
    public function adminproduksi()
    {
        $admin = admin::where('adm_role', '=', '3')->get();
        return view('beckend.super_admin.AdminDivisi_adminproduksi', ['admin' => $admin]);
    }


    public function tambah()
    {

        return view('beckend.super_admin.TambahAdminDivisi');
    }
    public function simpan(Request $request)
    {

        // dd($request->all());
        $admin = admin::create([

            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'alamat' => $request->alamat,
            'adm_role' => $request->adm_role,
            'status' => "Nonaktif"

        ]);


        return redirect()->route('superadmin.admindivisi.adminproduksi')->with('status', 'Data Admin Berhasil Ditambahkan!');
        // ('beckend.super_admin.AdminDivisi_adminproduksi')
    }

    public function destroy(Admin $admin)
    {
        Admin::destroy($admin->id);
        return redirect('/superadmin.admindivisi.adminproduksi')->with('status', 'Data Admin Berhasil Dihapus!');
    }

    //send email
    public function sendEmail(Request $request)
    {
        //data
        $email = $request->email;
        $data = array(
            'name' => $request->name,
            'email_body' => $request->email_body,
            'username' => $request->username,
            'password' => $request->password,
            'email' => $request->email
        );

        DB::table('admin')->insert(
            [
                'email' => $request->email,
                'password' => $request->password,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'adm_role' => $request->adm_role,
                'status' => 'nonaktif'


            ]
        );

        //kirim email
        Mail::send('email_template', $data, function ($mail) use ($email) {
            $mail->to($email, 'no-replay')
                ->subject("Regitrasi Akun");
            $mail->from('iinasnawiyah@gmail.com', 'Project Management System');
        });



        //cek kegagalan
        if (Mail::failures()) {
            return "Gagal Mengirim Email";
        }
        return redirect()->route('superadmin.admindivisi')->with('status', 'Data Admin dan Email Berhasil Dikirim');
    }

    public function detail($id)
    {
        $admin = admin::where('id', $id)->get();
        return view('beckend.super_admin.Admin-detail', ['admin' => $admin]);
    }

    public function gantistatus($id)
    {
        $admin = DB::table('admin')->where('id', $id)->first();
        return view('beckend.super_admin.Admin-gantistatus', ['admin' => $admin]);
    }
    
    public function simpanstatus(Request $request, $id)
    {
        $admin = DB::table('admin')->where('id', $id)->update(
            [
                'status' => $request->status,
                'updated_at' => $request->updated_at,

            ]
        );

        return back()->with('status', 'Status berhasil dirubah');
    }
}
