<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class User extends Controller
{
    public function index()
    {
        return view('beckend.super_admin.User');
    }

    public function tambah()
    {

        return view('beckend.super_admin.user_tambah');
    }

    public function simpan(Request $request)
    {

        // dd($request->all());
        $user = users::create([

            'user_role' => $request->user_role,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name' => $request->name,
            'status' => $request->status,
        ]);


        return redirect()->route('superadmin.user')->with('status', 'Data Client Baru Berhasil Ditambahkan!');
    }
    public function client()
    {
        $client = users::where('user_role', '=', '1')->get();
        return view('beckend.super_admin.client', ['client' => $client]);
    }

    public function subclient()
    {
        $client = users::where('user_role', '=', '2')->get();
        return view('beckend.super_admin.subclient', ['client' => $client]);
    }

    public function gantistatus($id)
    {
        $DataClient = DB::table('users')->where('id', $id)->first();
        return view('beckend.super_admin.Client-gantistatus', ['DataClient' => $DataClient]);
    }

    public function simpanstatus(Request $request, $id)
    {
        // dd($request->all());
        $DataClient = DB::table('users')->where('id', $id)->update(
            [
                'status' => $request->status,
                'updated_at' => $request->updated_at,

            ]
        );

        return redirect(route('superadmin.user.client', $id))->with('popup', 'Status Client Berhasil dirubah');
    }

    public function gantistatusub($id)
    {
        $DataClient = DB::table('users')->where('id', $id)->first();
        return view('beckend.super_admin.Subclient-gantistatus', ['DataClient' => $DataClient]);
    }

    public function simpanstatusub(Request $request, $id)
    {
        // dd($request->all());
        $DataClient = DB::table('users')->where('id', $id)->update(
            [
                'status' => $request->status,
                'updated_at' => $request->updated_at,

            ]
        );

        return redirect(route('superadmin.user.subclient', $id))->with('popup', 'Status Client Berhasil dirubah');
    }
    public function detail($id)
    {
        $users = users::where('id', $id)->get();
        return view('beckend.super_admin.Client-detail', ['users' => $users]);
    }
}
