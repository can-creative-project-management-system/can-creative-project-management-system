<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use \Exception;

use Illuminate\Http\Request;

class UserAuthController extends Controller

{


    public function __construct()
    {
    }

    public function index()
    {
        return view('client.Dashboard');
    }

    public function formLogin()
    {
        return view('auth.login_user');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        // $username = $request->email;
        // dd($username);


        // dd($request->all());
        $akun = DB::table('users')->where('email', $request->email)->first();
        // dd($akun);
        if ($akun->email != null) {
            if (Hash::check($request->password, $akun->password)) {
                Session::put('is_admin', false);
                if ($akun->user_role === 1) {
                    Auth::guard('userclient');
                    Session::put('id', $akun->id);
                    // Session::put('id_user', $akun->id);
                    Session::put('nama', $akun->name);
                    // echo "admin utama" . "nama : " . Session::get('nama');
                    return redirect()->route('userclient');
                }
                if ($akun->user_role === 2) {
                    Auth::guard('usersubclient');
                    Session::put('id', $akun->id);
                    // Session::put('id_user', $akun->id);
                    Session::put('nama', $akun->name);
                    return redirect()->route('usersubclient');
                }
            } else {
                return redirect('user/login')->with('failedLogin', 'Password salah');
            }
        } else {
            return redirect('user/login')->route('login')->with('failedLogin', 'Email salah');
        }
        return redirect('user/login')->route('login')->with('failedLogin', 'Email salah');
    }
}
