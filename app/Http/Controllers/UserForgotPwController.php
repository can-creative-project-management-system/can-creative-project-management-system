<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Model\admin;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;

class UserForgotPwController extends Controller
{
    public function index()
    {
        return view('forgotpw');
    }

    public function tampil(Request $request)
    {
        return view('forgotpw2');
    }

    //send email
    public function sendEmail(Request $request)
    {
        //data
        $email = $request->email;
        $data = array(
            'name' => $request->name,
            'email' => $request->email
        );


        //kirim email
        Mail::send('email_reset', $data, function ($mail) use ($email) {
            $mail->to($email, 'no-replay')
                ->subject("Reset Password Notification");
            $mail->from('iinasnawiyah@gmail.com', 'Project Management System');
        });



        //cek kegagalan
        if (Mail::failures()) {
            return "Gagal Mengirim Email";
        }
        return redirect()->route('forgotpw')->with('status', 'Email Berhasil Dikirim');
    }

    public function Emailreset(Request $request)
    {

        //simpan pw baru
        $newPassword = $request->newPassword;
        $email = $request->email;
        $confirmPassword = $request->confirmPassword;
        //    dd($newPassword, $confirmPassword);
        if ($newPassword === $confirmPassword) {
            $change = DB::table('admin')
                ->where('email', $email)
                ->update(['password' => Hash::make($newPassword)]);

            // return redirect('user/forgotpw2')->with('success', 'Berhasil Mengganti Password');
        } elseif ($newPassword != $confirmPassword) {

            // return view('user.forgotpw2', ['email' => $email])->with('failedPass', 'Password Konfirmasi Tidak Sama');
        }

        //data
        $email = $request->email;
        $data = array(
            'newPassword' => $request->newPassword,
            'confirmPassword' => $request->confirmPassword,
            'email' => $request->email
        );

        //kirim email
        Mail::send('Confirm_resetpw', $data, function ($mail) use ($email) {
            $mail->to($email, 'no-replay')
                ->subject("Your password was reset");
            $mail->from('iinasnawiyah@gmail.com', 'Project Management System');
        });

        //cek kegagalan
        if (Mail::failures()) {
            return "Gagal Mengirim Email";
        }
        return redirect()->route('forgotpw2')->with('status', ' Berhasil Mengganti Password');
    }

    // public function forgotPw(Request $request)
    // {
    //     $newPassword = $request->newPassword;
    //     $no_telp = $request->no_telp;
    //     $confirmPassword = $request->confirmPassword;
    //     //    dd($newPassword, $confirmPassword);
    //     if ($newPassword === $confirmPassword) {
    //         $change = DB::table('users')
    //             ->where('no_telp', $no_telp)
    //             ->update(['password' => Hash::make($newPassword)]);

    //         return redirect('user/login')->with('success', 'Berhasil Mengganti Password');
    //     } elseif ($newPassword != $confirmPassword) {

    //         return view('user.forgotpw', ['no_telp' => $no_telp])->with('failedPass', 'Password Konfirmasi Tidak Sama');
    //     }
    // }
}
