<?php

namespace App\Http\Controllers\AdminAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ticket extends Controller
{
    public function index()
    {
        $ticket = DB::table('tickets')
        ->join('project', 'tickets.id_project', '=', 'project.id_project')
        ->select('tickets.*', 'project.nama', 'project.logo')
        ->get();
        return view('beckend.admin_account.Ticket', ['ticket' => $ticket]);
    }
    public function detail($id_ticket)
    {
        $project = DB::table('tickets')
        ->where('id_ticket', $id_ticket)
        ->join('project', 'tickets.id_project', '=', 'project.id_project')
        ->select('tickets.*', 'project.nama', 'project.logo')
        ->get();
        return view('beckend.admin_account.Tiket-detail', ['project' => $project]);
    }
    public function gantistatus($id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->first();
        return view('beckend.admin_account.Tiket-gantistatus', ['Ticket' => $Ticket]);
    }
    
    public function simpanstatus(Request $request, $id_ticket)
    {
        // dd($request->all());
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->update(
            [
                'status_penanganan' => $request->status_penanganan,
                'updated_at' => $request->updated_at,

            ]
        );

        return redirect(route('AdminAccount.tiket', $id_ticket))->with('status', 'Status Ticket Berhasil dirubah');
    }
    public function jawab($id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->first();
        return view('beckend.admin_account.Tiket-gantistatus', ['Ticket' => $Ticket]);
    }
    
    public function simpanjawab(Request $request, $id_ticket)
    {
        // dd($request->all());
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->update(
            [
                'status_penanganan' => $request->status_penanganan,
                'updated_at' => $request->updated_at,

            ]
        );

        return redirect(route('AdminAccount.tiket', $id_ticket))->with('status', 'Status Ticket Berhasil dirubah');
    }

}
