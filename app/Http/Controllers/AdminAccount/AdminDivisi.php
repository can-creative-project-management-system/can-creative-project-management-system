<?php

namespace App\Http\Controllers\AdminAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\admin;
use Illuminate\Support\Facades\DB;

class AdminDivisi extends Controller
{
    public function index(){
        return view ('beckend.admin_account.AdminDivisi');
    }
    public function adminaccount(){
        $admin = admin::where('adm_role', '=', '1')->get();
        return view('beckend.admin_account.AdminDivisi_adminaccount', ['admin' => $admin]);
    }
    public function admininfo(){
        $admin = admin::where('adm_role', '=', '4')->get();
        return view('beckend.admin_account.AdminDivisi_admininfo', ['admin' => $admin]);

    }
    public function adminproduksi(){
        $admin = admin::where('adm_role', '=', '3')->get();
        return view('beckend.admin_account.AdminDivisi_adminproduksi', ['admin' => $admin]);
    }

    public function gantistatus($id)
    {
        $admin = DB::table('admin')->where('id', $id)->first();
        return view('beckend.admin_account.Admin-gantistatus', ['admin' => $admin]);
    }
    
    public function simpanstatus(Request $request, $id)
    {
        $admin = DB::table('admin')->where('id', $id)->update(
            [
                'status' => $request->status,
                'updated_at' => $request->updated_at,

            ]
        );

        return back()->with('status', 'Status berhasil dirubah');
    }
    public function detail($id)
    {
        $admin = admin::where('id', $id)->get();
        return view('beckend.admin_account.Admin-detail', ['admin' => $admin]);
    }
}
