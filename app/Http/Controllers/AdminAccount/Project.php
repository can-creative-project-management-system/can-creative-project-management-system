<?php

namespace App\Http\Controllers\AdminAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\project_model;
use App\information_model;
use App\report_model;

use Illuminate\Support\Facades\DB;

class Project extends Controller
{
    public function index(){
        return view ('beckend.admin_account.Project');
    }
    public function praproduksi(){
        $proyek = project_model::where('status', '=', 'Pra')->get();
        return view('beckend.admin_account.Project-praproduksi', ['proyek' => $proyek]);
    }
    public function produksi(){
        $proyek = project_model::where('status', '=', 'Produksi')->get();
        return view('beckend.admin_account.Project-produksi', ['proyek' => $proyek]);
    }
    public function pascaproduksi(){
        $proyek = project_model::where('status', '=', 'Pasca')->get();
        return view('beckend.admin_account.Project-pascaproduksi', ['proyek' => $proyek]);
    }

    public function tambah_project()
    {

        return view('beckend.admin_account.TambahProject');
    }

    public function simpan_project(Request $request)
    {
        $file = $request->file('mou');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $destinationPath = 'mou';
        $file->move($destinationPath, $nama_file);

        $logo = $request->file('logo');

        $nama_logo = time() . "_" . $logo->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $destinationPath = 'logo';
        $logo->move($destinationPath, $nama_logo);

        $admin = project_model::create([

            'nama' => $request->nama,
            'due' => $request->due,
            'detail' => $request->detail,
            'status' => $request->status,
            'mou' => $nama_file,
            'logo' => $nama_logo


        ]);


        return redirect('superadmin.project')->with('status', 'Project Berhasil Dibuat!');
    }

    public function client($id_project)
    {
        $proyek = DB::table('project')->where('id_project', $id_project)->first();
        return view('beckend.admin_account.tambahclient', ['proyek' => $proyek]);
    }
    public function update(Request $request, $id_project)
    {

        // dd($request->all());
        $proyek = DB::table('project')->where('id_project', $id_project)->update(
            [
                'id_client' => $request->id_client,
            ]
        );
        return redirect()->route('superadmin.project')->with('statusSuccess', 'Data Berhasil Di Update');
    }

    public function report($id_project)
    {

        // $proyek = DB::table('report')->where('id_project', $id_project)->get();
        $proyek = DB::table('report')->where('id_project', $id_project)
            ->orderBy('id_report', 'DESC')
            ->get();
        return view('beckend.admin_account.report', ['proyek' => $proyek]);
    }
    public function detail($id_project)
    {
        $project = project_model::where('id_project', $id_project)->get();
        return view('beckend.admin_account.Project-detail', ['project' => $project]);
    }
    public function gantistatus($id_project)
    {
        $proyek = DB::table('project')->where('id_project', $id_project)->first();
        return view('beckend.admin_account.Project-gantistatus', ['proyek' => $proyek]);
    }
    
    public function simpanstatus(Request $request, $id_project)
    {
        // dd($request->all());
        $proyek = DB::table('project')->where('id_project', $id_project)->update(
            [
                'status' => $request->status,
                'updated_at' => $request->updated_at,

            ]
        );

        return back()->with('status', 'Status berhasil dirubah');
    }
    public function info()
    {
        $info = DB::table('information')->get();
        return view('beckend.admin_account.info', ['info' => $info]);
    }

    public function tambah_info()
    {

        return view('beckend.admin_account.tambah_info');
    }

    public function simpan_info(Request $request)
    {

        $ticket = information_model::create([
            'module' => $request->module,
            'keterangan' => $request->keterangan,
        ]);


        return redirect()->route('AdminAccount.informasi')->with('status', 'Informasi Baru Berhasil Ditambahkan!');
    }
    public function delete($id_report)
    {
        DB::table('report')->where('id_report', $id_report)->delete();
        return back()->with('status', 'Data Report Berhasil Dihapus');
    }
    public function tambahreport()
    {
        return view('beckend.admin_account.report_tambah');
    }
    public function simpanreport(Request $request)
    {

        try {
            $admin = report_model::create([

                'id_project' => $request->id_project,
                'subject' => $request->subject,
                'changelog' => $request->changelog,
                'link' => $request->link,
            ]);
            $datareport = report_model::where("subject", $request->subject)->first();

            if ($request->hasFile('file')) {
                $files = $request->file('file');

                foreach ($files as $file) {
                    $nama_file = time() . "_" . $file->getClientOriginalName();

                    $destinationPath = 'report';
                    $file->move($destinationPath, $nama_file);

                    $admin = DB::table('file')->insert([
                        'id_report' => $datareport->id_report,
                        'file' => $nama_file,
                    ]);
                }

                return view('beckend.admin_account.report_tambah', ['admin' => $admin])->with('pop up', 'Project Berhasil Dibuat!');
            }
            return 'Empty Files';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function detailreport($id_report)
    {
        // dd($id_report);
        $project = report_model::where('id_report', $id_report)
        ->join('project', 'report.id_project', '=', 'project.id_project')
        ->select('report.*', 'project.nama')
        ->first();

        return view('beckend.admin_account.Report-detail', ['DataAdmin' => $project]);
    }

    public function downloadreport($file)
    {
        return response()->download('report/'.$file);
    }

   
}
