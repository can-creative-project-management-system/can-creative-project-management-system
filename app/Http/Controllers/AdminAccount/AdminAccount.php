<?php

namespace App\Http\Controllers\AdminAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
class AdminAccount extends Controller
{
    public function index(){
        return view ('beckend.admin_account.Dashboard');
    }

    public function user()
    {
        return view('beckend.admin_account.User');
    }

    public function tambahuser()
    {

        return view('beckend.admin_account.user_tambah');
    }

    public function simpanuser(Request $request)
    {

        // dd($request->all());
        $user = users::create([

            'user_role' => $request->user_role,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name' => $request->name,
            'status' => $request->status,
        ]);


        return redirect()->route('superadmin.user')->with('status', 'Data Admin Berhasil Ditambahkan!');
        // ('beckend.super_admin.AdminDivisi_adminproduksi')
    }
    public function client()
    {
        $client = users::where('user_role', '=', '1')->get();
        return view('beckend.admin_account.client', ['client' => $client]);
    }

    public function subclient()
    {
        $client = users::where('user_role', '=', '2')->get();
        return view('beckend.admin_account.subclient', ['client' => $client]);
    }

    public function gantistatus($id)
    {
        $DataClient = DB::table('users')->where('id', $id)->first();
        return view('beckend.admin_account.Client-gantistatus', ['DataClient' => $DataClient]);
    }
    
    public function simpanstatus(Request $request, $id)
    {
        // dd($request->all());
        $DataClient = DB::table('users')->where('id', $id)->update(
            [
                'status' => $request->status,
                'updated_at' => $request->updated_at,

            ]
        );

        return redirect(route('AdminAccount.user.client', $id))->with('popup', 'Status Client Berhasil dirubah');
    }
    public function detail($id)
    {
        $users = users::where('id', $id)->get();
        return view('beckend.admin_account.Client-detail', ['users' => $users]);
    }

}
