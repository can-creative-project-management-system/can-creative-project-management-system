<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;


class AkunClient extends Controller
{


    //tampil profil
    public function update($id)
    {

        $users = DB::table('users')->where('id', $id)->first();
        return view('frontend/user/Profil', ['users' => $users]);
    }

    //tampil update profil updateProfil
    public function updateProfil($id)
    {
        $users = DB::table('users')->where('id', $id)->first();
        return view('frontend/user/edit', ['users' => $users]);
    }

    //simpan updat eprofil
    public function updateProcess(Request $request, $id)
    {

        // dd($request->all());
        DB::table('users')->where('id', Session::get('id'))->update(
            [
                'name' => $request->name,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'tlp' => $request->tlp,

            ]
        );
        return redirect()->route('edit.data', $id)->with('statusSuccess', 'Data Berhasil Di Update');
    }


    //tampil update Password
    public function updatePw($id)
    {
        $users = DB::table('users')->where('id', $id)->first();
        return view('password.ubahPassword', ['users' => $users]);
        // return view('user.profil.akun');

    }

    //Update password
    public function updatePassword(Request $request, $id)
    {

        $sandi_baru = $request->sandi_baru;
        if ($sandi_baru === $request->konfirmasi) {
            DB::table('users')->where('id', $id)->update([
                'password' => Hash::make($request->sandi_baru),
            ]);
            return redirect()->back()->with("editpw", "Berhasil Ganti Password");
        } else {
            return redirect()->back()->with("failedpw", "Gagal Ganti Password");
        }
    }
}
