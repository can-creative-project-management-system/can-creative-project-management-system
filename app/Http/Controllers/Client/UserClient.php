<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\users;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;


class UserClient extends Controller
{
    public function index()
    {
        return view('beckend.client.Dashboard');
    }

    public function tambah()
    {

        return view('beckend.client.user_tambah');
    }

    public function simpan(Request $request)
    {

        $user = users::create([

            'user_role' => $request->user_role,
            'id_client' => Session::get('id'),
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name' => $request->name,
            'status' => $request->status,
        ]);


        return redirect()->route('client.user.simpan')->with('status', 'Data Berhasil Ditambahkan!');
    }

    // //send email
    public function sendEmail(Request $request)
    {
        //data
        $email = $request->email;
        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => $request->password,
            'status' => $request->status
        );

        DB::table('users')->insert(
            [

                'email' => $request->email,
                'password' => $request->password,
                'name' => $request->name,
                'user_role' => $request->user_role,
                'status' => 'nonaktif'


            ]
        );

        //kirim email
        Mail::send('email_user', $data, function ($mail) use ($email) {
            $mail->to($email, 'no-replay')
                ->subject("Regitrasi Akun");
            $mail->from('iinasnawiyah@gmail.com', 'Project Management System');
        });



        //cek kegagalan
        if (Mail::failures()) {
            return "Gagal Mengirim Email";
        }
        return redirect()->route('client.user.simpan')->with('status', 'Email Berhasil Dikirim! Silahkan cek email...');
    }

    // public function index()
    // {
    //     return view('beckend.client.Dashboard');
    // }

    // public function tambah()
    // {

    //     return view('beckend.client.user_tambah');
    // }

    // public function simpan(Request $request)
    // {

    //     $user = users::create([

    //         'user_role' => $request->user_role,
    //         'id_client' => Session::get('id'),
    //         'email' => $request->email,
    //         'password' => Hash::make($request->password),
    //         'name' => $request->name,
    //         'status' => $request->status,
    //     ]);


    //     return redirect()->route('client.sublient')->with('status', 'Data Admin Berhasil Ditambahkan!');
    // }
    // public function gantistatus($id)
    // {
    //     $DataClient = DB::table('users')->where('id', $id)->first();
    //     return view('beckend.client.gantistatussub', ['DataClient' => $DataClient]);
    // }
    
    // public function simpanstatus(Request $request, $id)
    // {
    //     $DataClient = DB::table('users')->where('id', $id)->update(
    //         [
    //             'status' => $request->status,
    //             'updated_at' => $request->updated_at,
    //         ]
    //     );

    //     return redirect(route('SubClient'))->with('popup', 'Status Sublient Berhasil dirubah');
    // }

  
}
