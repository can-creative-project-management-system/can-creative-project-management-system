<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\tiket;
use App\Ticket_model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class Ticket extends Controller
{
    public function Ticket()
    {
        $ticket = DB::table('tickets')->where('id_client', Session::get('id'))->get();
        return view('beckend.client.Ticket', ['ticket' => $ticket]);
    }

    public function tambah()
    {

        return view('beckend.client.ticket_tambah');
    }

    public function simpan(Request $request)
    {
        if ($request->hasFile('file')) {

            $files = $request->file('file');
            foreach ($files as $file) {
                $nama_file = time() . "_" . $file->getClientOriginalName();
                $destinationPath = 'ticket';
                $file->move($destinationPath, $nama_file);

                $ticket = Ticket_model::create([

                    'status_penanganan' => $request->status_penanganan,
                    'id_client' => Session::get('id'),
                    'id_project' => $request->id_project,
                    'subject' => $request->subject,
                    'detail_ticket' => $request->detail,
                    'divisi' => $request->divisi,
                    'urgent_status' => $request->urgent_status,
                    'file' => $nama_file,
                ]);
            }
        }

        return redirect()->route('client.ticket')->with('status', 'Data Admin Berhasil Ditambahkan!');
    }
    public function detail(Request $request, $ticket)
    {
        $ticket = DB::table('tickets')->where('id_ticket', $ticket)
            ->join('project', 'project.id_project', '=', 'tickets.id_project')
            ->join('users', 'users.id', '=', 'project.id_client')
            ->first();
        return view('beckend.client.ticket_detail', compact('ticket'));
    }
    // public function Ticket()
    // {
    //     $ticket = DB::table('tickets')->where('id_client', Session::get('id'))->get();
    //     return view('beckend.client.Ticket', ['ticket' => $ticket]);
    // }

    // public function tambah()
    // {

    //     return view('beckend.client.ticket_tambah');
    // }

    // public function simpan(Request $request)
    // {

    //     $file = $request->file('file');
    //     $nama_file = time() . "_" . $file->getClientOriginalName();
    //     $destinationPath = 'ticket';
    //     $file->move($destinationPath, $nama_file);

    //     $ticket = tiket::create([

    //         'status_penanganan' => $request->status_penanganan,
    //         'id_client' => Session::get('id'),
    //         'id_project' => $request->id_project,
    //         'subject' => $request->subject,
    //         'detail' => $request->detail,
    //         'divisi' => $request->divisi,
    //         'urgent_status' => $request->urgent_status,
    //         'file' => $request->nama_file,
    //     ]);


    //     return redirect()->route('client.ticket')->with('status', 'Data Admin Berhasil Ditambahkan!');
    // }

    // public function detail(Request $id_ticket)
    // {
    //     $ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->first();
    //     return view('beckend.client.ticket_detail', compact('ticket'));
    // }
}
