<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\project_model;
use App\report_model;
use Illuminate\Support\Facades\DB;

class Project extends Controller
{
    public function Project()
    {
        
        return view('beckend.client.Project_saya');
    }

    public function client($id_project)
    {
        $proyek = DB::table('project')->where('id_project', $id_project)->first();
        return view('beckend.client.tambahclient', ['proyek' => $proyek]);
       

    }
    public function update(Request $request, $id_project)
    {

        // dd($request->all());
        DB::table('project')->where('id_project', $id_project)->update(
            [
                'id_subclient' => $request->id_subclient,

            ]
        );
        return redirect()->route('project.saya', $id_project)->with('statusSuccess', 'Data Berhasil Di Update');
    }

    public function report($id_project)
    {
        $proyek = DB::table('report')->where('id_project', $id_project)->orderBy('id_report', 'DESC')->get();
        return view('beckend.client.report', ['proyek' => $proyek]);
    }

    public function detail($id_project)
    {
        $project = project_model::where('id_project', $id_project)->get();
        return view('beckend.client.Project-detail', ['project' => $project]);
    }
    public function info()
    {
        $info = DB::table('information')->get();
        return view('beckend.client.info', ['info' => $info]);
    }
    public function detailreport($id_report)
    {
        $project = report_model::where('id_report', $id_report)
        ->join('project', 'report.id_project', '=', 'project.id_project')
        ->select('report.*', 'project.nama')
        ->first();

        return view('beckend.client.Report-detail', ['DataAdmin' => $project]);
    }

    public function downloadreport($file)
    {
        return response()->download('report/'.$file);
    }

}
