<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class SubClient extends Controller
{
    public function subClient()
    {
        $subclient = DB::table('users')->where('id_client', Session::get('id'))->get();
        return view('beckend.client.Subclient', ['subclient' => $subclient]);
    }
    
}
