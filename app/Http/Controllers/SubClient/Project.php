<?php

namespace App\Http\Controllers\SubClient;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\project_model;
use App\report_model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Project extends Controller
{
    public function Project()
    {
        return view('beckend.subclient.Project_saya');
    }

    public function praproduksi(){
        $proyek = project_model::where('status', '=', 'Pra')->where('id_subclient', Session::get('id'))->get();
        return view('beckend.subclient.Project-praproduksi', ['proyek' => $proyek]);
    }
    public function produksi(){
        $proyek = project_model::where('status', '=', 'Produksi')->where('id_subclient', Session::get('id'))->get();
        return view('beckend.subclient.Project-produksi', ['proyek' => $proyek]);
    }
    public function pascaproduksi(){
        $proyek = project_model::where('status', '=', 'Pasca')->where('id_subclient', Session::get('id'))->get();
        return view('beckend.subclient.Project-pascaproduksi', ['proyek' => $proyek]);
    }
    public function detail($id_project)
    {
        $project = project_model::where('id_project', $id_project)->get();
        return view('beckend.subclient.Project-detail', ['project' => $project]);
    }
    public function report($id_project)
    {

        $proyek = DB::table('report')->where('id_project', $id_project)->orderBy('id_report', 'DESC')->get();
        return view('beckend.subclient.report', ['proyek' => $proyek]);
    }
    public function info()
    {
        $info = DB::table('information')->get();
        return view('beckend.subclient.info', ['info' => $info]);
    }

    public function detailreport($id_report)
    {
        $project = report_model::where('id_report', $id_report)
        ->join('project', 'report.id_project', '=', 'project.id_project')
        ->select('report.*', 'project.nama')
        ->first();

        return view('beckend.subclient.Report-detail', ['DataAdmin' => $project]);
    }

    public function downloadreport($file)
    {
        return response()->download('report/'.$file);
    }

}
