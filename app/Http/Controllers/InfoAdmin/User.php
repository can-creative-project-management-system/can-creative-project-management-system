<?php

namespace App\Http\Controllers\InfoAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\users;

class User extends Controller
{
    public function index()
    {
        return view('beckend.admin_info.User');
    }

    public function client()
    {
        $client = users::where('user_role', '=', '1')->get();
        return view('beckend.admin_info.client', ['client' => $client]);
    }

    public function subclient()
    {
        $client = users::where('user_role', '=', '2')->get();
        return view('beckend.admin_info.subclient', ['client' => $client]);
    }
    public function detail($id)
    {
        $users = users::where('id', $id)->get();
        return view('beckend.admin_info.Client-detail', ['users' => $users]);
    }

}
