<?php

namespace App\Http\Controllers\InfoAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\admin;
use Illuminate\Support\Facades\DB;

class AdminDivisi extends Controller
{
    public function index(){
        return view ('beckend.admin_info.AdminDivisi');
    }
    public function adminaccount(){
        $admin = admin::where('adm_role', '=', '1')->get();
        return view('beckend.admin_info.AdminDivisi_adminaccount', ['admin' => $admin]);
    }
    public function admininfo(){
        $admin = admin::where('adm_role', '=', '4')->get();
        return view('beckend.admin_info.AdminDivisi_admininfo', ['admin' => $admin]);

    }
    public function adminproduksi(){
        $admin = admin::where('adm_role', '=', '3')->get();
        return view('beckend.admin_info.AdminDivisi_adminproduksi', ['admin' => $admin]);
    }
    public function detail($id)
    {
        $admin = admin::where('id', $id)->get();
        return view('beckend.admin_info.Admin-detail', ['admin' => $admin]);
    }
}
