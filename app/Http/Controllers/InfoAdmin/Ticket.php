<?php

namespace App\Http\Controllers\InfoAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class Ticket extends Controller
{
    public function index()
    {
        $ticket = DB::table('tickets')
        ->join('project', 'tickets.id_project', '=', 'project.id_project')
        ->select('tickets.*', 'project.nama', 'project.logo')
        ->where('divisi', '=', 'info')
        ->get();
        return view('beckend.admin_info.Ticket', ['ticket' => $ticket]);
    }

    public function Ticket()
    {
        $ticket = DB::table('tickets')->where('id_client', Session::get('id'))->get();
        return view('beckend.admin_info.ticket_info', ['ticket' => $ticket]);
    }

    public function detail($id_ticket)
    {
        $project = DB::table('tickets')
        ->where('id_ticket', $id_ticket)
        ->join('project', 'tickets.id_project', '=', 'project.id_project')
        ->select('tickets.*', 'project.nama', 'project.logo')
        ->get();
        return view('beckend.admin_info.Tiket-detail', ['project' => $project]);
    }
    // public function detail(Request $request, $ticket)
    // {
    //     $ticket = DB::table('tickets')->where('id_ticket', $ticket)
    //         ->join('project', 'project.id_project', '=', 'tickets.id_project')
    //         ->join('users', 'users.id', '=', 'project.id_client')
    //         ->first();
    //     return view('beckend.client.ticket_detail', compact('ticket'));
    // }
    public function gantistatus($id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->first();
        return view('beckend.admin_info.Tiket-gantistatus', ['Ticket' => $Ticket]);
    }
    
    public function simpanstatus(Request $request, $id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->update(
            [
                'status_penanganan' => $request->status_penanganan,
                'updated_at' => $request->updated_at,
            ]
        );

        return redirect(route('AdminInfo.ticket', $id_ticket))->with('status', 'Status Ticket Berhasil dirubah');
    }

    public function jawab($id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->get();
        return view('beckend.admin_info.Jawab_ticket', ['Ticket' => $Ticket]);
    }

    public function answer(Request $request, $id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->update(
            [
                'jawaban' => $request->jawaban,
                'updated_at' => $request->updated_at,
            ]
        );

        return redirect(route('superadmin.tiket', $id_ticket))->with('popup', 'Ticket telah di jawab');
    }

}
