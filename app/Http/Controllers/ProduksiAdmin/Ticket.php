<?php

namespace App\Http\Controllers\ProduksiAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Ticket extends Controller
{
    public function index()
    {
        $ticket = DB::table('tickets')
            ->join('project', 'tickets.id_project', '=', 'project.id_project')
            ->select('tickets.*', 'project.nama', 'project.logo')
            ->where('divisi', '=', 'support')
            ->get();
        return view('beckend.admin_produksi.Ticket', ['ticket' => $ticket]);
    }
    public function detail($id_ticket)
    {
        $project = DB::table('tickets')
            ->where('id_ticket', $id_ticket)
            ->join('project', 'tickets.id_project', '=', 'project.id_project')
            ->select('tickets.*', 'project.nama', 'project.logo')
            ->get();
        return view('beckend.admin_produksi.Tiket-detail', ['project' => $project]);
    }
    public function gantistatus($id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->first();
        return view('beckend.admin_produksi.Tiket-gantistatus', ['Ticket' => $Ticket]);
    }

    public function simpanstatus(Request $request, $id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->update(
            [
                'status_penanganan' => $request->status_penanganan,
                'updated_at' => $request->updated_at,
            ]
        );

        return redirect(route('AdminProduksi.dataticket', $id_ticket))->with('popup', 'Status Ticket Berhasil dirubah');
    }
    public function Ticket()
    {
        $ticket = DB::table('tickets')->where('id_client', Session::get('id'))->get();
        return view('beckend.admin_produksi.ticket_produksi', ['ticket' => $ticket]);
    }

    public function jawab($id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->get();
        return view('beckend.admin_produksi.Jawab_ticket', ['Ticket' => $Ticket]);
    }

    public function answer(Request $request, $id_ticket)
    {
        $Ticket = DB::table('tickets')->where('id_ticket', $id_ticket)->update(
            [
                'jawaban' => $request->jawaban,
                'updated_at' => $request->updated_at,
            ]
        );

        return redirect(route('superadmin.tiket', $id_ticket))->with('popup', 'Ticket telah di jawab');
    }
}
