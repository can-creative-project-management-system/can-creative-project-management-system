<?php

namespace App\Http\Controllers\ProduksiAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\users;
use App\admin;
class AdminProduksi extends Controller
{
    public function index()
    {
        return view('beckend.admin_produksi.Dashboard');
    }

    public function user()
    {
        return view('beckend.admin_produksi.User');
    }

    public function client()
    {
        $client = users::where('user_role', '=', '1')->get();
        return view('beckend.admin_produksi.client', ['client' => $client]);
    }

    public function subclient()
    {
        $client = users::where('user_role', '=', '2')->get();
        return view('beckend.admin_produksi.subclient', ['client' => $client]);
    }

    public function admin()
    {
        return view('beckend.admin_produksi.AdminDivisi');
    }

    public function adminaccount()
    {
        $admin = admin::where('adm_role', '=', '1')->get();
        return view('beckend.admin_produksi.AdminDivisi_adminaccount', ['admin' => $admin]);
    }

    public function admininfo()
    {

        $admin = admin::where('adm_role', '=', '4')->get();
        return view('beckend.admin_produksi.AdminDivisi_admininfo', ['admin' => $admin]);
    }

    public function adminproduksi()
    {
        $admin = admin::where('adm_role', '=', '3')->get();
        return view('beckend.admin_produksi.AdminDivisi_adminproduksi', ['admin' => $admin]);
    }
    public function detail($id)
    {
        $admin = admin::where('id', $id)->get();
        return view('beckend.admin_produksi.Admin-detail', ['admin' => $admin]);
    }

    public function detailsub($id)
    {
        $users = users::where('id', $id)->get();
        return view('beckend.admin_produksi.Client-detail', ['users' => $users]);
    }


}
