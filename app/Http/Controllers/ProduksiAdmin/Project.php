<?php

namespace App\Http\Controllers\ProduksiAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\project_model;
use App\information_model;
use Illuminate\Support\Facades\DB;
use App\report_model;
class Project extends Controller
{
    public function index()
    {
        return view('beckend.admin_produksi.Project');
    }

    public function praproduksi()
    {
        $proyek = project_model::where('status', '=', 'Pra')->get();
        return view('beckend.admin_produksi.Project-praproduksi', ['proyek' => $proyek]);
    }
    public function produksi()
    {
        $proyek = project_model::where('status', '=', 'Produksi')->get();
        return view('beckend.admin_produksi.Project-produksi', ['proyek' => $proyek]);
    }
    public function pascaproduksi()
    {
        $proyek = project_model::where('status', '=', 'Pasca')->get();
        return view('beckend.admin_produksi.Project-pascaproduksi', ['proyek' => $proyek]);
    }

    public function report($id_project)
    {

        $proyek = DB::table('report')->where('id_project', $id_project)->orderBy('id_report', 'DESC')->get();
        return view('beckend.admin_produksi.report', ['proyek' => $proyek]);
    }
    public function gantistatus($id_project)
    {
        $proyek = DB::table('project')->where('id_project', $id_project)->first();
        return view('beckend.admin_produksi.Project-gantistatus', ['proyek' => $proyek]);
    }
    public function simpanstatus(Request $request, $id_project)
    {
        // dd($request->all());
        $proyek = DB::table('project')->where('id_project', $id_project)->update(
            [
                'status' => $request->status,
                'updated_at' => $request->updated_at,

            ]
        );

        return back()->with('status', 'Status berhasil dirubah');
    }
    public function detail($id_project)
    {
        $project = project_model::where('id_project', $id_project)->get();
        return view('beckend.admin_produksi.Project-detail', ['project' => $project]);
    }
    public function info()
    {
        $info = DB::table('information')->get();
        return view('beckend.admin_produksi.info', ['info' => $info]);
    }

    public function tambah_info()
    {

        return view('beckend.admin_produksi.tambah_info');
    }

    public function simpan_info(Request $request)
    {

        $ticket = information_model::create([
            'module' => $request->module,
            'keterangan' => $request->keterangan,
        ]);


        return redirect()->route('AdminProduksi.informasi')->with('status', 'Informasi Baru Berhasil Ditambahkan!');
    }
    public function delete($id_report)
    {
        DB::table('report')->where('id_report', $id_report)->delete();
        return back()->with('status', 'Data Report Berhasil Dihapus');
    }
    public function detailreport($id_report)
    {
        $project = report_model::where('id_report', $id_report)
        ->join('project', 'report.id_project', '=', 'project.id_project')
        ->select('report.*', 'project.nama')
        ->first();

        return view('beckend.admin_produksi.Report-detail', ['DataAdmin' => $project]);
    }

    public function downloadreport($file)
    {
        return response()->download('report/'.$file);
    }
    public function tambahreport()
    {
        return view('beckend.admin_produksi.report_tambah');
    }
    public function simpanreport(Request $request)
    {

        try {
            $admin = report_model::create([

                'id_project' => $request->id_project,
                'subject' => $request->subject,
                'changelog' => $request->changelog,
                'link' => $request->link,
            ]);
            $datareport = report_model::where("subject", $request->subject)->first();

            if ($request->hasFile('file')) {
                $files = $request->file('file');

                foreach ($files as $file) {
                    $nama_file = time() . "_" . $file->getClientOriginalName();

                    $destinationPath = 'report';
                    $file->move($destinationPath, $nama_file);

                    $admin = DB::table('file')->insert([
                        'id_report' => $datareport->id_report,
                        'file' => $nama_file,
                    ]);
                }

                return view('beckend.admin_produksi.report_tambah', ['admin' => $admin])->with('pop up', 'Project Berhasil Dibuat!');
            }
            return 'Empty Files';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    
    
}
