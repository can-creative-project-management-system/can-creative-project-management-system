<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class information_model extends Model
{
    protected $table = "information";
    protected $fillable = ["id", "module", "keterangan"];
    protected $hidden = ['created_at', 'updated_at'];
}
