<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tiket extends Model
{
    protected $table = "tickets";
    public $timestamps = true;
    protected $guarded = ['updated_at'];
}
