<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    protected $table = "admin";
    public $timestamps = true;
    protected $guarded = ['updated_at'];
    
    public function komentar()
    {
        return $this->hasMany(Komentar::class);
    }
}
