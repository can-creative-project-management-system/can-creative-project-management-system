<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project_model extends Model
{
    protected $table = "project";
    public $timestamps = true;
    protected $guarded = ['updated_at'];
}
