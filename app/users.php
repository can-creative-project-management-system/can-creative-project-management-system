<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class users extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $guarded = ['updated_at'];


    public function komentar()
    {
        return $this->hasMany(Komentar::class);
    }

    // public function getCreatedAtAttribute()
    // {
    //     return \Carbon\Carbon::parse($this->attributes['created_at'])
    //         ->format('d, M Y H:i');
    // }
    // public function getUpdatedAtAttribute()
    // {
    //     return \Carbon\Carbon::parse($this->attributes['updated_at'])
    //         ->diffForHumans();
    // }

    protected $appends = ['published'];

    public function getPublishedAttribute()
    {

        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']))->diffForHumans();
    }
}
